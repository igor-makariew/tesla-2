<?php
    require_once('core/controller.php');

    class Admin extends Controller{

        public $core;

        public function __construct($core){
            parent::__construct($core);

            $this->core = $core;

            // подключение стилей и скриптов
            $this->add_css('modules/admin/web/css/bootstrap.css');
            $this->add_css('modules/admin/web/css/bootstrap-theme.css');
            $this->add_css('modules/admin/web/css/starter-template.css');
            $this->add_css('modules/admin/web/css/style.css');
            $this->add_css('modules/admin/web/css/my-style.css');


            $this->add_js('modules/admin/web/js/jquery.min.js');
            $this->add_js('modules/admin/web/js/bootstrap.min.js');

            $this->add_fonts('modules/admin/web/fonts/glyphicons-halflings-regular.woff');
            $this->add_fonts('modules/admin/web/fonts/glyphicons-halflings-regular.woff2');
//            $this->add_js('https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
//            $this->add_js('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js');
            //$this->add_js('modules/admin/web/js/npm.js');

            //задаем путь шаблона
            $this->set_layout('modules/admin/layout/admin_layout/admin_layout.php');
        }

        function actionIndex(){
            // start проверка роли пользователя
            if($this->core->get_core()['user_model']->array_user['author'] == 1) {
                $this->params['title'] = "Admin index - {$this->core->get_core()['user_model']->array_user['author']}";

                $this->params['avatar'] = isset($this->core->get_core()['user_model']->get_array_user()['author'])?$this->core->get_core()['user_model']->get_array_user()['author']:'no avatar';
                $this->params['get_array_user'] = $this->core->get_core()['user_model']->get_array_user();

            }
            // end проверка роли пользователя

            $this->params['title'] = "Admin index - {$this->core->get_core()['user_model']->array_user['author']}";

            $this->params['avatar'] = isset($this->core->get_core()['user_model']->get_array_user()['author'])?$this->core->get_core()['user_model']->get_array_user()['author']:'no avatar';
            $this->params['get_array_user'] = $this->core->get_core()['user_model']->get_array_user();

            return $this->render('views/admin/index.php');

        }

        function actionDeleteavatar(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $delete_avatar = $_POST['image'];

            //modules/admin/web/images/avatars/1_igor makarev/8k.jpg
            //'C:\OSPanel\domains\tesla\modules\admin\web\images\girls\500.jpg'

            if($this->core->get_core()['images_model']->delete_update_params_avatar($this->core->get_core()['user_model']->get_array_user()['id'])){
                if(unlink($delete_avatar)){
                    echo json_encode(false);
                }else{
                    echo json_encode(true);
                }
            }else{
                echo json_encode(true);
            }

        }

        function actionUpdatepersonaldate(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $name = $_POST['name'];
            $old_year = $_POST['old_year'];
            $city = $_POST['city'];
            $language = $_POST['language'];
            $image = $_POST['image'];

            $array_true_false = [];
            if(!empty($name) || !empty($old_year) || !empty($city) || !empty($language || !empty($image))){

                foreach($_POST as $key => $value){
                    if($key == 'name' && $value != ''){
                        if($this->core->get_core()['personal_date_model']->update_name_author($this->core->get_core()['user_model']->get_array_user()['id'], $value)){
                            $array_true_false[$key] = 'true';
                        }
                    }else if($value != '' && $key != 'name'){
                        if($this->core->get_core()['personal_date_model']->update_value($this->core->get_core()['user_model']->get_array_user()['id'], $key, $value)){
                            $array_true_false[$key] = 'true';
                        }
                    }else{
                        $array_true_false[$key] = 'false';
                    }
                }
                if($this->core->get_core()['personal_date_model']->return_true_false($array_true_false)){
                    echo json_encode(false);
                }else{
                    echo json_encode(true);
                }
            }
            //echo json_encode($_POST);
        }

        function actionGetpersonaldate(){
            $_POST = json_decode(file_get_contents('php://input'), true);

//            $array_number = ['0','1','2','3','4','5','6','7','8','9'];
//            $array = explode(':', $_POST);
//            // длина слова
//            $len = mb_strlen($array[1]);
//            $argement = [];
//            // выбираем из массива числовое значение
//            for($i = 0; $i < $len; $i++){
//                foreach ($array_number as $key => $value) {
//                    if($value == mb_substr($array[1], $i, 1)){
//                        $argement[$key] = $value;
//                    }
//                }
//            }
//            $stroka = implode($argement);
//            $chislo = (int)$stroka;

            echo json_encode($this->core->get_core()['personal_date_model']->get_user($_POST['id']));
        }

        function actionCreateimage(){
            //путь к файлу
            $dir = 'modules/admin/web/images/avatars/'.$this->core->get_core()['user_model']->get_array_user()['id'].'_'.$this->core->get_core()['user_model']->get_array_user()['login'];

            if(!file_exists($dir)){
                mkdir($dir);
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    move_uploaded_file($_FILES['file']['tmp_name'], $dir.'/'.$_FILES['file']['name']);
                    if($this->core->get_core()['images_model']->set_avatar(
                        $this->core->get_core()['user_model']->get_array_user()['id'],
                        $_FILES['file']['name'],
                        $dir.'/'.$_FILES['file']['name']
                    )){
                        echo json_encode(1);
                    }
                }else{
                    echo json_encode(true);
                }
            }else{
                if(isset($_FILES['file']['tmp_name'])){
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){//var_dump($_FILES);
                        move_uploaded_file($_FILES['file']['tmp_name'], $dir.'/'.$_FILES['file']['name']);

                        $param_avatar_old = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);

                        if($param_avatar_old['avatar'] ==  1){  //var_dump($param_avatar_old);

                            if($this->core->get_core()['images_model']->delete_update_params_avatar($this->core->get_core()['user_model']->get_array_user()['id'])){
                                if($this->core->get_core()['images_model']->set_avatar(
                                    $this->core->get_core()['user_model']->get_array_user()['id'],
                                    $_FILES['file']['name'],
                                    $dir.'/'.$_FILES['file']['name']
                                    )
                                ){
                                    $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                                    unlink($param_avatar_old['path_image']);
                                    echo json_encode($param_avatar_new);
                                }
                            }else{
                                echo json_encode(true);
                            }
                        }else{
                            if($this->core->get_core()['images_model']->set_avatar(
                                $this->core->get_core()['user_model']->get_array_user()['id'],
                                $_FILES['file']['name'],
                                $dir.'/'.$_FILES['file']['name']
                            )){
                                $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                                echo json_encode($param_avatar_new);
                            }
                        }
                    }
                }else{
                    $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                    echo json_encode($param_avatar_new);
                }
            }
        }

        function actionGetavatar(){
            $param_avatar = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);

            echo json_encode($param_avatar);
        }

        function actionListuser(){
            $this->params['title'] = 'Список пользователей';

            return $this->render('views/admin/listuser.php');
        }

        function actionGetlistuser(){
            //$_POST = json_encode(file_get_contents('php://input'), true);
            echo json_encode($this->core->get_core()['listuser_model']->get_list_user());
        }

        function actionDeletelistuser(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            if($this->core->get_core()['listuser_model']->delete_user($_POST['id'])){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }

        function actionUpdateuser(){
            $_POST = json_decode(file_get_contents('php://input'), true);
            //echo json_encode($this->core->get_core()['listuser_model']->update_user((array)$_POST));
            if($this->core->get_core()['listuser_model']->update_user((array)$_POST)){
                echo json_encode(false);
            }else{
                echo json_encode(true);
            }
            //echo json_encode((array)$_POST);
        }

        //sort column
        function actionSortcolumn(){
            $_POST = json_decode(file_get_contents('php://input'), true);

//            echo json_encode($_POST);
             echo json_encode($this->core->get_core()['listuser_model']->sort_column($_POST['sort'], $_POST['column']));
        }

        //settings users
        function actionSettings(){
            $this->params['title'] = 'настройки страницы пользователя';
            $this->params['id_user'] = $this->core->get_core()['user_model']->get_array_user()['id'];

            return $this->render('views/admin/setting.php');
        }

        function actionSetpersonaldata(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            if($_POST['author'] != '' || $_POST['login'] != '' || $_POST['email'] != '' || $_POST['old_year'] != ''
                || $_POST['telephone'] != '' || $_POST['city'] != '' || $_POST['language'] != '' || $_POST['gender']){
                echo json_encode($this->core->get_core()['personal_date_model']->update_data_table_users($_POST));
            }

//            if($_POST['old_year'] != '' || $_POST['telephone'] != '' || $_POST['city'] != '' || $_POST['language'] != '' || $_POST['gender']){
//                echo json_encode($this->core->get_core()['personal_date_model']->update_data_table_personal_date($_POST));
//                //echo json_encode($_POST);
//            }
        }

        //CRUD
        function actionCrud(){
            $this->params['title'] = 'CRUD';

            return $this->render('views/admin/crud.php');
        }

        function actionMigration(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            //var_dump($this->core->get_core()['listuser_model']->list_tables_in_db());

            // набор строк для sql
            $str_sql_table = '';
            foreach ($_POST['array'] as $key => $value){
                if($value['Length'] == "") {
                    $str_sql_table .= $value['Name'] ." ".$value['Type']. ",";
                } else {
                    $str_sql_table .= $value['Name'] ." ".$value['Type']. " (" . $value['Length'] . "),";
                }
            }

            $sql_table = "CREATE TABLE " . "`".$_POST['name_table']."`" . " (". $str_sql_table . ")";
            $sql_table = substr($sql_table, 0, -2);
            $sql_table .= ")";

            if($this->core->get_core()['migration_model']->set_migration($sql_table)){
                echo json_encode($this->core->get_core()['migration_model']->list_tables_in_db());
            }else{
                echo json_encode('no');
            }
        }

        function actionCrudupdate(){
            $this->params['title'] = 'Update CRUD';
            $this->params['lists_tables'] = $this->core->get_core()['migration_model']->list_tables_in_db();

            return $this->render('views/admin/crudupdate.php');
        }

        function actionListsfieldtable(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            echo json_encode($this->params['lists_tables'] = $this->core->get_core()['migration_model']->get_fields_tables($_POST['table_name']));
        }

        function actionCruddelete(){
            $this->params['title'] = 'Delete CRUD';

            return $this->render('views/admin/cruddelete.php');
        }

        function actionListtablesindb(){
            echo json_encode($this->core->get_core()['migration_model']->list_tables_in_db());
        }

        function actionDeletetable(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            if($this->core->get_core()['migration_model']->delete_table($_POST['name_table'])){
                echo json_encode($this->core->get_core()['migration_model']->list_tables_in_db());
            }else{
                echo json_encode('Что то пошло не так! Поробуйте позже!!!');
            }
        }

        function actionConfirmpassword(){
            $_POST = json_decode(file_get_contents('php://input'), true);
            $array_personal_data = $this->core->get_core()['personal_date_model']->get_user($_POST['id']);
            if($array_personal_data['uniq_code'] == $_POST['uniq_code']){
                if($this->core->get_core()['personal_date_model']->update_password_user($_POST['id'], $_POST['password'])){
                    $to = $array_personal_data['email'];
                    $author = $array_personal_data['author'];
                    $subject = 'Change Password OK';
                    //$message = "Уважаемый пользователь {$author} вы усспешно изменили пароль!";
                    $message = "TEST-TEST-TEST";
                    //echo var_dump(mail($to, $subject, $message));
                    if(mail($to, $subject, $message)){
                        echo json_encode(true);
                    }else{
                        echo json_encode(false);
                    }
                }else{
                    echo json_encode(false);
                }
            }else{
                echo json_encode(false);
            }

        }

        function actionPostuniqcode(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $random_string = $this->core->get_core()['personal_date_model']->generateRandomString(30);
            if($this->core->get_core()['personal_date_model']->update_personal_date_uniq_code($_POST['id'], $random_string)){
                $users_data = $this->core->get_core()['personal_date_model']->get_user($_POST['id']);
            }

            // отправка письма пользователю с уникальным кодом

            $to = $users_data['email'];
            $author = $users_data['author'];
            $subject = 'Change Password';
            //$message = "Уважаемый пользователь {$author} скопируйте и вставьте уникальный код {$random_string} в поле Uniq Code при смене пароля. Иначе вы не сможете изменить пароль";
            $message = "TEST-TEST-TEST";
            //echo var_dump(mail($to, $subject, $message));
            if(mail($to, $subject, $message)){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }

        function actionDownloadfile(){
            // по скачиванию файла
            $oSpreadsheet = new Spreadsheet();

            // TODO что-то делаем с $oSpreadsheet заполняем данными

            $oWriter = IOFactory::createWriter($oSpreadsheet, 'Xlsx');
            $sFilename = '123.xlsx';

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $sFilename . '"');
            header('Cache-Control: max-age=0');

            $oWriter->save('php://output');
            exit;
        }

        function actionProjects(){
            $this->params['title'] = 'Projects';

            return $this->render('views/admin/projects.php');

        }

        function actionGetallprojects(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $this->core->get_core()['projects_model']->set_page($_POST['page']);
            $this->core->get_core()['projects_model']->set_on_page(10);

            $this->core->get_core()['projects_model']->set_shift(
            //$this->core->get_core()['pagination_model']->get_array_page($_SESSION['sort'], $_SESSION['column']),
                $this->core->get_core()['projects_model']->get_array_page(),
                $this->core->get_core()['projects_model']->get_page(),
                $this->core->get_core()['projects_model']->get_on_page());

            $this->core->get_core()['projects_model']->set_count($this->core->get_core()['projects_model']->count_projects());

            $this->core->get_core()['projects_model']->set_page_count(ceil(
                $this->core->get_core()['projects_model']->count_projects()['count(*)'] /
                $this->core->get_core()['projects_model']->get_on_page()));

            $this->params['pages'] = $this->core->get_core()['projects_model']->get_page_count();

            //выборка из базы данных
            $this->params['projects'] = $this->core->get_core()['projects_model']->get_shift();
            // end pagination

            // подсчет строк в массиве
            $_SESSION['count_str'] = $this->core->get_core()['projects_model']->count_id_array($_POST['page']);

            echo json_encode($this->core->get_core()['projects_model']->get_shift());
        }

        function actionGetsearchprojects(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $this->core->get_core()['projects_model']->set_page($_POST['sort_page']);

            $this->core->get_core()['projects_model']->set_on_page(10);

            $this->core->get_core()['projects_model']->set_shift(
                $this->core->get_core()['projects_model']->get_array_page($_POST['column'], $_POST['search']),
                $this->core->get_core()['projects_model']->get_page(),
                $this->core->get_core()['projects_model']->get_on_page());

            $this->core->get_core()['projects_model']->set_count($this->core->get_core()['projects_model']->count_projects());

            $this->core->get_core()['projects_model']->set_page_count(ceil(
                $this->core->get_core()['projects_model']->count_projects()['count(*)'] /
                $this->core->get_core()['projects_model']->get_on_page()));

            $this->params['pages'] = $this->core->get_core()['projects_model']->get_page_count();

            //выборка из базы данных
            $this->params['projects'] = $this->core->get_core()['projects_model']->get_shift();
            // end pagination

            echo json_encode($this->core->get_core()['projects_model']->get_shift());
        }

        function actionGetcountpagination(){
            $this->core->get_core()['projects_model']->set_on_page(10);

            $this->core->get_core()['projects_model']->set_count($this->core->get_core()['pagination_model']->count_projects());

            $this->core->get_core()['projects_model']->set_page_count(ceil(
                $this->core->get_core()['projects_model']->count_projects()['count(*)'] /
                $this->core->get_core()['projects_model']->get_on_page()));

            $this->params['pages'] = $this->core->get_core()['projects_model']->get_page_count();

            echo json_encode($this->core->get_core()['projects_model']->get_page_count());

        }

        function actionGetcountsortpagination(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $this->core->get_core()['projects_model']->set_on_page(10);

            $this->core->get_core()['projects_model']->set_count($this->core->get_core()['pagination_model']->count_projects());

            $this->core->get_core()['projects_model']->set_page_count(ceil(
                $this->core->get_core()['projects_model']->count_sort_projects($_POST['column'], $_POST['search'])['count(*)'] /
                $this->core->get_core()['projects_model']->get_on_page()));

            echo json_encode($this->core->get_core()['projects_model']->get_page_count());

        }

        function actionGetpagination(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $this->core->get_core()['projects_model']->set_page($_POST['page']);
            $this->core->get_core()['projects_model']->set_on_page(10);

            $this->core->get_core()['projects_model']->set_shift(
                //$this->core->get_core()['pagination_model']->get_array_page($_POST['search'], $_POST['column']),
                $this->core->get_core()['projects_model']->get_array_page(),
                $this->core->get_core()['projects_model']->get_page(),
                $this->core->get_core()['projects_model']->get_on_page());

            $this->core->get_core()['projects_model']->set_count($this->core->get_core()['projects_model']->count_projects());

            $this->core->get_core()['projects_model']->set_page_count(ceil(
                $this->core->get_core()['projects_model']->count_projects()['count(*)'] /
                $this->core->get_core()['projects_model']->get_on_page()));

            $this->params['pages'] = $this->core->get_core()['projects_model']->get_page_count();

            //выборка из базы данных
            $this->params['projects'] = $this->core->get_core()['projects_model']->get_shift();
            // end pagination
            // подсчет строк в массиве
            $_SESSION['count_str'] = $this->core->get_core()['projects_model']->count_id_array($_POST['page']);

            echo json_encode($this->core->get_core()['projects_model']->get_shift());
        }

        function actionGetcountidarray(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            echo json_encode($this->core->get_core()['projects_model']->count_id_array($_POST['count_str']));
        }

        function actionGetselectonid(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            echo json_encode($this->core->get_core()['projects_model']->select_projects_on_id($_POST['id']));
        }

        function actionUpdateproject(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            echo json_encode($this->core->get_core()['projects_model']->update_projects_on_id($_POST));
        }

        function actionDeleteproject(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            echo json_encode($this->core->get_core()['projects_model']->delete_projects_on_id($_POST['id']));
        }

        // action new article
        function actionArticles(){
            $this->params['title']='NEW ARTICLE';

            return $this->render('views/admin/articles.php');
        }

        function actionArticletest(){
            //echo json_encode('Что то пошло не так! Поробуйте позже!!!', JSON_UNESCAPED_UNICODE);
            echo json_encode('Что то пошло не так! Поробуйте позже!!!');
        }

        //load more 10
        function actionLoadmore(){
            $_POST = json_decode(file_get_contents('php://input'), true);
            if($this->core->get_core()['projects_model']->count_projects()['count(*)'] > $_POST['load_more']){
                echo json_encode($this->core->get_core()['projects_model']->load_more($_POST['load_more']));
            }else{
                echo json_encode($this->core->get_core()['projects_model']->load_more($_POST['load_more']));
            }

        }

        function actionInsertintoproject(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            if($this->core->get_core()['projects_model']->insert_into_project($_POST)){
                echo json_encode(true);
            } else{
                echo json_encode(false);
            }
        }


    }
