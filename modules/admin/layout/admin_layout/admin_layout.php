<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="http://bootstrap-3.ru/assets/ico/favicon.ico">

    <title><?= $title; ?></title>

    <?= $this->create_css_style() ?>
    <?= $this->create_fonts() ?>
    <?= $this->create_js_script() ?>
    <?php // $this->create_fonts()?>

  </head>
  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><?= $core->get_core()['user_model']->get_array_user()['author'] ?></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="?action=admin/index">Index</a></li>
            <?php if($core->get_core()['user_model']->array_user['role'] == 1):?>
                <li><a href="?action=admin/listuser">Users</a></li>
            <?php endif;?>
            <li><a href="?action=admin/articles">New Article</a></li>
            <li><a href="?action=admin/projects">New Project</a></li>
            <li><a href="?action=admin/email">E-mail</a></li>
            <li><a href="?action=admin/settings">Setting</a></li>
            <li><a href="?action=logout">Выход</a></li>
          </ul>
        </div>>
      </div>
    </div>

    <div class="container" style="padding: 25px;">
        <div class="row">
            <?= $content; ?>
        </div>
    </div>

    <?php // $this->create_js_script() ?>

  </body>
</html>