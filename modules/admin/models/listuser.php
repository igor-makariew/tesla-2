<?php
    require_once ('core/model.php');

    class Listuser extends Model{

        //get list user
        function get_list_user(){
            $query = "SELECT * FROM `users`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $result_array;
        }

        // delete user
        function delete_user($id){
            $query = sprintf("DELETE FROM `users` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query);
            if($result){
                return true;
            }else{
                return false;
            }
        }

        // update user
        function update_user($array){
            foreach($array['array'] as $key => $value){
                $query = sprintf("UPDATE `users` SET `{$key}` = '{$value}' WHERE `id` = %s", mysqli_real_escape_string($this->get_link(), $array['id']));
                $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
                if($result){
                    return true;
                }else{
                    return false;
                }
            }
        }

        // sort column
        function sort_column($sort, $column){
            $query = sprintf("SELECT * FROM `users` ORDER BY `%s` %s", mysqli_real_escape_string($this->get_link(), $column), mysqli_real_escape_string($this->get_link(), $sort));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }
    }
