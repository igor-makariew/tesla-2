<?php
    require_once ('core/model.php');

    class Migration extends Model{

        function list_tables_in_db(){
            $db = 'tesla';
            $query = "SHOW TABLES FROM $db";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function set_migration($table){
            $query = $table;
            $result = mysqli_query($this->get_link(),$query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return fasle;
            }
        }

        function get_fields_tables($name_tables){
            $query = "SHOW COLUMNS FROM $name_tables";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function delete_table($name_table){
            $query = "DROP TABLE $name_table";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }
    }
