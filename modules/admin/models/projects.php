<?php
    require_once ('core/model.php');

    class Projects extends Model{
        public $page;
        public $on_page;
        public $shift;
        public $count;
        public $page_count;
        public $sort;

        function get_all_projects(){
            $query = "SELECT * FROM `projects`";
            $results = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($results, MYSQLI_ASSOC);
            return $array_result;
        }

        function get_search_projects($column, $search){
            $query = sprintf("SELECT * FROM `projects` WHERE `%s` LIKE '%s'", mysqli_real_escape_string($this->get_link(), $column),
                                mysqli_real_escape_string($this->get_link(),$search.'%'));
            $results = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($results, MYSQLI_ASSOC);
            return $array_result;

        }

        // передаем нумерацию страницы
        function set_page($page){
            $this->page = $page;
        }
        // получаем нумерацию
        function get_page(){
            return $this->page;
        }
        // расположение на странице количества элементов
        function set_on_page($on_page){
            $this->on_page =  $on_page;
        }
        // получение данных на странице количества элементов
        function get_on_page(){
            return $this->on_page;
        }
        // смещение по массиву
        function set_shift($array, $offset, $length){
            $this->shift = array_slice($array, $length*($offset-1), $length*1);
        }

        // получение смещения по массиву
        function get_shift(){
            return $this->shift;
        }

        // количество элементов в массиве
        function set_count($array){
            $this->count = $array;
        }

        // получение количества элементов в массиве
        function get_count(){
            return $this->count;
        }

        // количество страниц
        function set_page_count($page_count){
            $this->page_count = $page_count;
        }

        // получение количества страниц
        function get_page_count(){
            return $this->page_count;
        }

        // сортировка массива
        function get_array_page($column = null, $search = null){
            if(!empty($column) && !empty($search)){
                $query = sprintf("SELECT * FROM `projects` WHERE `%s` LIKE '%s'", mysqli_real_escape_string($this->get_link(), $column),
                                    mysqli_real_escape_string($this->get_link(),$search.'%'));
            }else{
                $query = "SELECT * FROM `projects`";
            }
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }
        // количество элементов в массиве
        function count_projects(){
            $query = "SELECT count(*) FROM `projects`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function count_sort_projects($column, $search){
            $query = sprintf("SELECT count(*) FROM `projects` WHERE `%s` LIKE '%s'", mysqli_real_escape_string($this->get_link(), $column),
                                mysqli_real_escape_string($this->get_link(),$search.'%'));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function select_projects_on_id($id){
            $query = sprintf("SELECT * FROM `projects` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
            return $array_result;
        }

        // update data table project
        function update_projects_on_id($array){
            $array_error = [];
            foreach($array as $key => $value){
                if($key == 'author' || $key == 'project' || $key == 'code' || $key == 'id'){
                    if($key != 'id' AND $value != $array['id']){
                        if($value != ''){
                            $query = sprintf("UPDATE `projects` SET `{$key}` = '{$value}' WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $array['id']));
                            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
                            if($result){
                                $array_error[$key] = $result;
                            }else{
                                $array_error[$key] = $result;
                            }
                        }
                    }
                }
            }

            return $this->return_true_false($array_error);
        }

        function return_true_false($array){
            $arr = [];
            foreach ($array as $key => $value){
                if($value == 'true'){
                    $arr[$key] = $value;
                }
            }
            if(count($arr) == count($array)) {     //count($arr) > 0 on count($arr) == count($array)
                return true;
            } else {
                return false;
            }
        }

        function delete_projects_on_id($id){
            $query = sprintf("DELETE FROM `projects` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function count_id_array($page){
            $number_page = ($page - 1) * 10;
            return $number_page;
        }

        //load more project

        function load_more($index){
            $index = $index*10;
            $array_projects = [];

            $all_projects = $this->get_all_projects();
            $array_projects = array_slice($all_projects, 0, $index - 1);
            return $array_projects;
        }

        // count (*) project
//        function count_projects(){
//            $query = "SELECT count(*) FROM `projects`";
//            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
//            $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
//            return $array_result;
//        }

        // insert into project
        function insert_into_project($array){
            $query = sprintf("INSERT INTO `projects`(`author`, `email`, `project`, `code`, `date`) VALUES('%s', '%s', '%s', '%s', '%s')",
                            mysqli_real_escape_string($this->get_link(), $array['author']), mysqli_real_escape_string($this->get_link(), $array['email']),
                            mysqli_real_escape_string($this->get_link(), $array['project']), mysqli_real_escape_string($this->get_link(), $array['code']),
                            mysqli_real_escape_string($this->get_link(), date("Y-m-d H:i:s")));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }
    }
