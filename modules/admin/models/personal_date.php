<?php
    require_once ('core/model.php');

    class Personaldate extends Model{
        //?
        function set_personal_date($array){
            $query = sprintf("INSERT INTO `personal_date` (`name`, `old_year`, `city`, `language`) VALUES ('%s', '%s', '%s', '%s')",
                                        mysqli_real_escape_string($this->get_link(), $array['name']), mysqli_real_escape_string($this->get_link(), $array['old_year']),
                                        mysqli_real_escape_string($this->get_link(), $array['city']), mysqli_real_escape_string($this->get_link(), $array['language'])
            );
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function get_personal_data($id){
            $query = sprintf("SELECT * FROM `personal_date` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            return $array_result;
        }

        function get_user($id){
            $query = sprintf("SELECT users.author, users.login, users.email, personal_date.old_year, personal_date.city, personal_date.language, personal_date.telephone, personal_date.gender, personal_date.uniq_code 
                                FROM `users` LEFT JOIN `personal_date` ON users.id = personal_date.id_users WHERE users.id = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            return $array_result;
        }

        function update_name_author($id, $name){
            $query = sprintf("UPDATE `users` SET `author` = '%s' WHERE `id` = '%s'",
                                    mysqli_real_escape_string($this->get_link(), $name), mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function update_value($id, $key, $value){
            $query = sprintf("UPDATE `personal_date` SET {$key} = '%s' WHERE `id` = '%s'",
                mysqli_real_escape_string($this->get_link(), $value), mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function return_true_false($array){
            $arr = [];
            foreach ($array as $key => $value){
                if($value == 'true'){
                    $arr[$key] = $value;
                }
            }
            if(count($arr) == count($array)) {     //count($arr) > 0 on count($arr) == count($array)
                return true;
            } else {
                return false;
            }
        }

        // update data table users
        function update_data_table_users($array){
            $array_error = [];
            foreach($array as $key => $value){
                if($key == 'author' || $key == 'login' || $key == 'email' || $key == 'id'){
                    if($key != 'id' AND $value != $array['id']){
                        if($value != ''){
                            $query = sprintf("UPDATE `users` SET `{$key}` = '{$value}' WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $array['id']));
                            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
                            if($result){
                                $array_error[$key] = $result;
                            }else{
                                $array_error[$key] = $result;
                            }
                        }
                    }
                }
            }

            foreach($array as $key => $value){
                if($key == 'old_year' || $key == 'telephone' || $key == 'city' || $key == 'language' || $key == 'gender' || $key == 'id'){
                    if($key != 'id' AND $value != $array['id']){
                        if($value != ''){
                            $query = sprintf("UPDATE `personal_date` SET `{$key}` = '{$value}' WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $array['id']));
                            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
                            if($result){
                                $array_error[$key] = $result;
                            }else{
                                $array_error[$key] = $result;
                            }
                        }
                    }
                }
            }

            return $this->return_true_false($array_error);
        }

        // update personal_date uniq code
        function update_personal_date_uniq_code($id, $uniq_code){
            $query = sprintf("UPDATE `personal_date` SET `uniq_code` = '%s' WHERE `id` = '%s'",
                                mysqli_real_escape_string($this->get_link(), $uniq_code), mysqli_real_escape_string($this->get_link(),$id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        // uodate password user
        function update_password_user($id, $password){
            $password_hash = password_hash($password, PASSWORD_DEFAULT);

            $query = sprintf("UPDATE `users` SET `password` = '%s' WHERE `id` = '%s'",  mysqli_real_escape_string($this->get_link(), $password_hash),
                                mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        // generate  Random String
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

    }
