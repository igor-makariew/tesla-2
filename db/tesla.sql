-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Авг 04 2020 г., 12:31
-- Версия сервера: 5.7.30-0ubuntu0.18.04.1
-- Версия PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tesla`
--

-- --------------------------------------------------------

--
-- Структура таблицы `avatar`
--

CREATE TABLE `avatar` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name_image` varchar(255) NOT NULL,
  `path_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `matter` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `author`, `matter`, `subject`, `date`) VALUES
(1, 'макарьев игорь', 'vue.js', '.then(response => (this.get_blog = response.data))\n                .catch(function (error) {\n                    console.log(error);\n                });', '2020-07-13 10:20:35'),
(2, 'макарьев игорь', 'vue2.js', '.then(response => (this.get_blog = response.data))\n                .catch(function (error) {\n                    console.log(error);\n                });', '2020-07-13 10:20:44'),
(3, 'vladimir', 'vue3.js', '.then(response => (this.get_blog = response.data))\n                .catch(function (error) {\n                    console.log(error);\n                });', '2020-07-13 10:20:52'),
(4, 'макарьев игорь', 'vue4.js', 'test_test', '2020-07-13 10:26:05'),
(5, 'макарьев игорь', 'vue3.js', 'get_blog', '2020-07-13 10:29:45'),
(6, 'макарьев игорь', 'vue4.js', 'set_post(){\n                axios({\n                   method:\'post\',\n                   headers:{\'Content-Type\': \'application/x-www-form-urlencoded\'},\n                    url: \'?action=site/blogform\',\n                    data:{\n                        \'author\': this.author,\n                        \'matter\': this.matter,\n                        \'subject\':this.subject\n                    }\n                }).then(response => (this.let_get_blog = response.data))\n                  .catch(function (error) {\n                        console.log(error);\n                    });\n            },', '2020-07-13 11:11:16'),
(7, 'макарьев игорь', 'vue5.js', 'this.get_blog', '2020-07-13 11:13:28'),
(8, 'макарьев игорь', 'vue4.js', 'add_blog', '2020-07-13 11:35:45'),
(9, 'add_blog', 'add_blog', 'add_blog', '2020-07-13 11:35:51'),
(10, 'макарьев игорь', 'vue4.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 12:59:50'),
(11, 'макарьев игорь', 'vue7.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 13:00:51'),
(12, 'макарьев игорь', 'vue7.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 13:01:45'),
(13, 'макарьев игорь', 'vue3.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 13:03:26'),
(14, 'макарьев игорь', 'vue3.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 13:03:38'),
(15, 'макарьев игорь', 'vue4.js', 'created: function () {\n    // _.debounce — это функция lodash, позволяющая ограничить то,\n    // насколько часто может выполняться определённая операция.\n    // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,\n    // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.\n    // Узнать больше о функции _.debounce (и её родственнице _.throttle),\n    // можно в документации: https://lodash.com/docs#debounce\n    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)\n  },', '2020-07-13 13:05:33');

-- --------------------------------------------------------

--
-- Структура таблицы `personal_date`
--

CREATE TABLE `personal_date` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `old_year` date NOT NULL,
  `city` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `personal_date`
--

INSERT INTO `personal_date` (`id`, `id_users`, `old_year`, `city`, `language`) VALUES
(1, 1, '1997-04-02', 'елец', 'русский');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `project` text NOT NULL,
  `code` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `author`, `email`, `project`, `code`, `date`) VALUES
(2, 'igor', 'igor.makariew@yandex.ru', 'test2test2', 'code2', '2020-07-01 00:00:00'),
(3, 'igor', 'igor.makariew@yandex.ru', 'tetst3test3', 'code3', '2020-07-02 00:00:00'),
(4, 'igor', 'igor.makariew@yandex.ru', 'tetst4test4', 'code4', '2020-07-03 00:00:00'),
(5, 'igor', 'igor.makariew@yandex.ru', 'status6', '<?php if(isset($params[\'error\'])):?>\r\n            <div class=\"alert alert-primary\" role=\"alert\">\r\n                Вы не заполнили поля!!!\r\n            </div>\r\n        <?php endif;?>', '2020-07-04 13:53:18'),
(6, 'igor', 'igor.makariew@yandex.ru', 'status6', '<?php if(isset($params[\'error\'])):?>\r\n            <div class=\"alert alert-primary\" role=\"alert\">\r\n                Вы не заполнили поля!!!\r\n            </div>\r\n        <?php endif;?>', '2020-07-04 13:53:26'),
(7, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 13:53:54'),
(8, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 13:55:58'),
(9, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 13:59:07'),
(10, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 13:59:42'),
(11, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 13:59:52'),
(12, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 14:00:03'),
(13, 'igor', 'igor.makariew@yandex.ru', 'status6', 'function add_user($author, $login, $email, $password, $role){\r\n            $password_hash = password_hash($password, PASSWORD_DEFAULT);\r\n\r\n            $query = sprintf(\"INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')\",\r\n                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),\r\n                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),\r\n                                    mysqli_real_escape_string($this->get_link(), USER));\r\n            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));\r\n            if($result){\r\n                return true;\r\n            }else{\r\n                return false;\r\n            }\r\n        }', '2020-07-04 14:00:52'),
(14, 'denis', 'denis@yandex.ru', 'date5', '<?php if(isset($params[\'error\'])):?>\r\n                    <div class=\"alert alert-primary\" role=\"alert\">\r\n                        Вы не заполнили поля!!!\r\n                    </div>\r\n                <?php endif;?>', '2020-07-04 15:20:09'),
(15, 'denis', 'denis@yandex.ru', 'date5', '<?php if(isset($params[\'error\'])):?>\r\n                    <div class=\"alert alert-primary\" role=\"alert\">\r\n                        Вы не заполнили поля!!!\r\n                    </div>\r\n                <?php endif;?>', '2020-07-04 15:20:44'),
(16, 'denis', 'denis@yandex.ru', 'date5', '<?php if(isset($params[\'error\'])):?>\r\n                    <div class=\"alert alert-primary\" role=\"alert\">\r\n                        Вы не заполнили поля!!!\r\n                    </div>\r\n                <?php endif;?>', '2020-07-04 15:21:09'),
(17, 'denis', 'denis@yandex.ru', 'status6', 'var_dump(!empty($submit));', '2020-07-04 15:27:10'),
(18, 'denis', 'denis@yandex.ru', 'status6', 'var_dump(!empty($submit));', '2020-07-04 15:27:28'),
(19, 'vladimir', 'vladimiir.makariew@yandex.ru', 'status7', 'var_dump(!empty($submit));', '2020-07-04 15:28:16'),
(20, 'vladimir', 'vladimiir.makariew@yandex.ru', 'date5', 'isset($params[\'error\'])', '2020-07-04 15:37:00'),
(21, 'igor', 'igor.makariew@yandex.ru', 'status77', '$this', '2020-07-04 15:40:25');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(5) NOT NULL,
  `avatar` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `author`, `login`, `email`, `password`, `role`, `avatar`) VALUES
(1, 'igor makarev', 'igor', 'igor.makariew@yandex.ru', '$2y$10$JbW3dEZteugqc.hkYMxrae4Dpc81kUo0DEo8Fnzzq6p4JC77ljPTC', '1', 0),
(2, 'denis kornev', 'denis', 'denis@yandex.ru', '$2y$10$lYx1ifBX8bJC0KuC0M5EAeDBx6OipbI/qe8f/BGoZHfXaDRD.W1/G', '2', 0),
(3, 'denis makarev', 'denis', 'denis.makarev@yandex.ru', '$2y$10$s.NCLbnIJjVRco3GiKxyR.sJRrvoElNCeyNa0LGWzXoDepIOCsVXS', '2', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `avatar`
--
ALTER TABLE `avatar`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `personal_date`
--
ALTER TABLE `personal_date`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `avatar`
--
ALTER TABLE `avatar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=601;
--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `personal_date`
--
ALTER TABLE `personal_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
