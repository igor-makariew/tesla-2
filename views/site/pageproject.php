<section class="ftco-section ftco-project" id="projects-section">
    <div class="container">
        <div class="row justify-content-center pb-5">
            <a href="?action=site/project&sort=<?= $_SESSION['sort']?>&page=<?= $_SESSION['page']?>&column=<?= $_SESSION['column']?>" class="nav-link">BACK PROJECTS</a>
            <div class="col-md-12 heading-section text-center ftco-animate">
                <h1 class="big big-2"><?= $params['array_page']['project']?></h1>
                <h2 class="mb-4"><span><?= $params['array_page']['project']?></span></h2>
                <p><?= substr($params['array_page']['code'], 0 , 30)?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="img ftco-animate d-flex justify-content-justify align-items-justify" >
                    <div class="text text-justify p-4">
                        <span><?= $params['array_page']['code']?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>