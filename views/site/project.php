<section class="ftco-section contact-section ftco-no-pb" xmlns="http://www.w3.org/1999/html">
    <div id="app">

    <div class="container">

        <template v-if="<?= $params['error_sql']?> ">
            <div class="alert alert-primary" role="alert">
                Попробуйте позже!!! Что то пошло не так!!!
            </div>
        </template>
        <?php var_dump($params['error'])?>
        <?php if($params['error']):?>
            <div class="alert alert-primary" role="alert">
                Вы не заполнили поля!!!
            </div>
        <?php endif;?>
<!--        <template v-if="--><?//= $params['error']?><!-- ">-->
<!--            <div class="alert alert-primary" role="alert">-->
<!--                Вы не заполнили поля!!!-->
<!--            </div>-->
<!--        </template>-->


        <form method="post" @submit="search">
            <div class="row justify-content-center">
                <div class="col-lg-9"><?php //var_dump($this->core->get_core()['user_model']->turn_number_into_string() <= 5)?>
                        <div class="input-group mb-3"><div class="col-lg-4 pt-2">
                        <?php if($this->core->get_core()['user_model']->turn_number_into_string() <= 5):?>
                            <h6><a href="#" class="nav-link" @click="add_project()">Add project</a></h6>
                        <?php endif;?>
                        </div>
                        <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2" v-model="str_search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-outline-warning" type="button" id="button-addon2" >SEARCH</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <template v-if="status_form_project">
            <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                <div class="container">
                    <div class="row no-gutters block-9">
                        <div class="col-md-12 order-md-last d-flex">
                            <form method="post" class="bg-light p-4 p-md-5 contact-form">
                                <div class="form-group">
                                    <label for="exampleInputLogin">Your Name</label>
                                    <input type="text" class="form-control" placeholder="Your Name" id="exampleInputLogin" name="author">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword">Your Email</label>
                                    <input type="email" class="form-control" placeholder="Your Email" id="exampleInputPassword" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputLogin">Your Name Project</label>
                                    <input type="text" class="form-control" placeholder="Your Name Project" id="exampleInputLogin" name="project">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword">Your Code </label>
                                    <textarea type="text" class="form-control" placeholder="Your Code" id="exampleInputPassword" name="code" rows="10"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Add_project" class="btn btn-primary py-3 px-5" name="add_project">
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </section>
        </template>

        <template v-if="hidden_dropdown">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <ul>
                        <li v-for="(item, index) in filteredNames" @click="str_search_project(index)"><a class="dropdown-item" href="#">{{item}}</a></li>
                    </ul>
                </div>
            </div>
        </template>

        <div class="row no-gutters block-9 pt-2">
            <div class="col-md-12 order-md-last d-flex">

                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"><a href="?action=site/project&sort=<?= $_SESSION['sort'] == 'ASC'?'DESC' : 'ASC' ?>&page=<?= $_SESSION['page']?>&column=author">AUTHOR</a></th>
                        <th scope="col"><a href="?action=site/project&sort=<?= $_SESSION['sort'] == 'ASC'?'DESC' : 'ASC' ?>&page=<?= $_SESSION['page']?>&column=email">EMAIL</a></th>
                        <th scope="col">PROJECT</th>
                        <th scope="col"><a href="?action=site/project&sort=<?= $_SESSION['sort'] == 'ASC'?'DESC' : 'ASC' ?>&page=<?= $_SESSION['page']?>&column=date">DATE</a></th>
<!--                        <th scope="col" @click="sort_date((let_get_params == 1)?2:1)"><a href="#">DATE</a></th>-->
                    </tr>
                    </thead>
                    <tbody>
<!--                     start отражениеданных из базы-->
                    <template v-if="!test[0]">
                    <?php foreach($params['projects'] as $key => $value):?>
                        <tr>
                            <th scope="row"><?= $_SESSION['count_str'] + $key + 1?></th>
                            <td><?= $value['author']?></td>
                            <td><?= $value['email']?></td>
                            <td><a href="?action=site/pageproject&page=<?= $value['id'] ?>" class="nav-link"><?= substr($value['project'], 0, 50)?></a></td>
                            <td><?= $value['date']?></td>
                        </tr>
                    <?php endforeach;?>
                    </template>

                    <template v-if="test[0]">
                        <tr v-for="(value, index) in test">
                            <th scope="row">{{index + 1}}</th>
                            <td>{{value.author}}</td>
                            <td>{{value.email}}</td>
                            <td>{{value.project}}</td>
                            <td>{{value.date}}</td>
                        </tr>
                    </template>
<!--                     end отражениеданных из базы-->
                    </tbody>
                </table>

            </div>
        </div>

        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center" >
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <?php for($i = 1; $i <= $params['pages']; $i++ ):?>
                    <li class="page-item <?= $_SESSION['page'] == $i?'active':''?>"><a class="page-link" href="?action=site/project&page=<?= $i?>&sort=<?= $_SESSION['sort']?>&column=<?= $_SESSION['column']?>"><?= $i?></a></li>
                <?php endfor;?>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>

    </div>

    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>

    let app = new Vue({
        el: "#app",
        data: {
            str_search:'',
            search_array:'',
            let_hidden_dropdown:false,
            project:'',
            all_search_array:'',
            test:[],
            status_form_project:false,
            let_sort_author:'',
            let_get_params: 1,
        },
        methods:{
            search:function (e) {
                e.preventDefault();
                axios
                    .get('?action=site/search')
                    .then(response => (this.post = response.data));
            },
            str_search_project(index){
                if(this.str_search != ''){
                    axios
                        .get('?action=site/searchproject&project='+this.search_array[index])
                        .then(response =>(this.project = response.data));
                }
            },
            add_project(){
                if(this.status_form_project){
                    return this.status_form_project = false;
                }else{
                    return this.status_form_project = true;
                }

            },
            sort_author(params){
                this.let_get_params = params;
                // запись переменной в строку
                axios
                    .get(`?action=site/project&sort=${this.let_get_params}`)
                    .then(response => (this.let_sort_author = response.data));
            },
            sort_email(){
                alert('ok');
            },
            sort_date(){
                alert('ok');
            }
        },
        mounted: function() {
            axios
                .post('?action=site/search')
                .then(response => (this.search_array = response.data));

            axios
                .post('?action=site/searchall')
                .then(response => (this.all_search_array = response.data));

        },
        computed: {
            filteredNames() {
                // return this.search_array.filter(name => {
                //     return name.indexOf(this.str_search) !== -1
                // });
                // чистим массив перед записью данных
                this.test.splice(0, this.test.length);

                this.all_search_array.forEach((value) => {
                    this.search_array.filter(name => {
                        return name.indexOf(this.str_search) !== -1
                    }).forEach((val, index) => {
                        if(value['project'] == val){console.log( val);
                            this.test[index] = value;
                        }
                    });
                });

                return this.test;
            },

            hidden_dropdown(){
                if(this.str_search != ''){
                    return this.let_hidden_dropdown = true;
                }else{
                    return this.let_hidden_dropdown = false;
                }
            }
        }
    });
</script>



<!--this.ajaxRequest = true;-->
<!--this.$http.post('http://localhost:3000/entry', {-->
<!--fname: this.fname,-->
<!--lname: this.lname,-->
<!--age: this.age-->
<!--}, function (data, status, request) {-->
<!--this.postResults = data;-->
<!---->
<!--this.ajaxRequest = false;-->
<!--});-->





<!--let currentObj = this;-->
<!---->
<!--this.axios.post('http://localhost:8000/yourPostApi', {-->
<!---->
<!--name: this.name,-->
<!---->
<!--description: this.description-->
<!---->
<!--})-->
<!---->
<!--.then(function (response) {-->
<!---->
<!--currentObj.output = response.data;-->
<!---->
<!--})-->


<!--new Vue({-->
<!--el: '#someId',-->
<!---->
<!--data: {-->
<!--id: ''-->
<!--},-->
<!---->
<!--methods: {-->
<!--search: function(e) {-->
<!--e.preventDefault();-->
<!---->
<!--var req = this.$http.get(-->
<!--'/api/search?id=' + this.id,-->
<!--function (data, status, request) {-->
<!--console.log(data);-->
<!--}-->
<!--);-->
<!--}-->
<!--}-->
<!--});-->
<!---->
<!---->
<!---->
<!---->

<!---->
<!---->
<!---->
<!--<div id="app">-->
<!--    <form v-on="submit: search">-->
<!--        <div class="form-group">-->
<!--            <input type="text" v-model="id" class="form-control" placeholder="id" required="required">-->
<!--        </div>-->
<!---->
<!--        <input type="submit" class="btn btn-default" value="Search">-->
<!--    </form>-->
<!--</div>-->
<!---->
<!--<script type="text/javascript">-->
<!---->
<!--    // get route url with blade-->
<!--    var url = "{{route('formRoute')}}";-->
<!---->
<!--    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');-->
<!---->
<!---->
<!--    var app = new Vue({-->
<!--        el: '#app',-->
<!--        data: {-->
<!--            id: '',-->
<!--            response: null-->
<!--        },-->
<!--        methods: {-->
<!--            search: function(event) {-->
<!--                event.preventDefault();-->
<!---->
<!--                var payload = {id: this.id};-->
<!---->
<!--                // send get request-->
<!--                this.$http.get(url, payload, function (data, status, request) {-->
<!---->
<!--                    // set data on vm-->
<!--                    this.response = data;-->
<!---->
<!--                }).error(function (data, status, request) {-->
<!--                    // handle error-->
<!--                });-->
<!--            }-->
<!--        }-->
<!--    });-->
<!--</script>-->