<section class="ftco-section contact-section ftco-no-pb">
    <div class="container">
        <div class="row no-gutters block-9">
            <?php if(isset($params['error'])):?>
                <div class="alert alert-primary" role="alert">
                    Вы не заполнили поля!!!
                </div>
            <?php endif;?>
            <div class="col-md-12 order-md-last d-flex">
                <form method="post" class="bg-light p-4 p-md-5 contact-form">
                    <div class="form-group">
                        <label for="exampleInputName">Your Name</label>
                        <input type="text" class="form-control" placeholder="Your Name" id="exampleInputName" name="author">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputLogin">Your Login</label>
                        <input type="text" class="form-control" placeholder="Your Login" id="exampleInputLogin" name="login">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Email address</label>
                        <input type="email" class="form-control" placeholder="Your Email" id="exampleInputEmail1" name="email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword">Email address</label>
                        <input type="password" class="form-control" placeholder="Your Password" id="exampleInputPassword" name="password">
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Registration" class="btn btn-primary py-3 px-5" name="submit">
                    </div>
                </form>

            </div>

        </div>
    </div>
</section>
