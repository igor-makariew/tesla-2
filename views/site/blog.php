<section class="ftco-section" id="blog-section">
    <div id="appblog">
    <div class="container">
        <?php if($this->core->get_core()['user_model']->turn_number_into_string() <= 5):?>
            <a href="#" @click="add_blog()">Add blog</a>
        <?php endif;?>
        <template v-if="status_form_blog">
            <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                <div class="container">
                    <template v-if="server">
                        <div class="alert alert-danger" role="alert">
                            Что то пошло не так!!! Попробуйте позже!!!
                        </div>
                    </template>
                    <div class="row no-gutters block-9">
                        <div class="col-md-12 order-md-last d-flex">

                            <form method="post" class="bg-light p-4 p-md-5 contact-form" @submit.prevent="submit_add_blog">
                                <div class="form-group">
                                    <label for="exampleInputLogin">Your Name</label>
                                    <input type="text" class="form-control" placeholder="Your Name" id="exampleInputLogin" v-model="author">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputMatter">Your Matter</label>
                                    <input type="text" class="form-control" placeholder="Your Matter" id="exampleInputMatter" v-model="matter">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputSubject">Your Subject </label>
                                    <textarea type="text" class="form-control" placeholder="Your Subject" id="exampleInputSubject" rows="10" v-model="subject"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" type="button" class="btn btn-primary py-3 px-5" value="Add_blog">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </section>
        </template>

        <div class="row no-gutters block-9 pt-2">
                <div class="col-md-12 order-md-last d-flex">

                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col"><a href="#" @click.prevent="sort_author" >AUTHOR</a></th>
                            <th scope="col"><a href="?action=site/blog&sort=<?= $_SESSION['sort'] == 'ASC'?'DESC' : 'ASC' ?>&page=<?= $_SESSION['page']?>&column=email">EMAIL</a></th>
                            <th scope="col">PROJECT</th>
                            <th scope="col" style="width: 120px;"><a href="?action=site/blog&sort=<?= $_SESSION['sort'] == 'ASC'?'DESC' : 'ASC' ?>&page=<?= $_SESSION['page']?>&column=date">DATE</a></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr v-for="value in let_get_blog">
                                <th scope="row">{{value.id}}</th>
                                <td>{{value.author}}</td>
                                <td>{{value.matter}}</td>
                                <td>{{value.subject}}</a></td>
                                <td>{{value.date}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
{{test}}
    </div>
    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    let app_blog = new Vue({
        el: "#appblog",
        data:{
            status_form_blog:false,
            author: '',
            matter: '',
            subject: '',
            server:false,
            let_get_blog:'',
            let_set_blog:'',
            sort:'DESC',
            column:'author',
            test:'',
        },
        watch:{

        },
        created: function () {
            // _.debounce — это функция lodash, позволяющая ограничить то,
            // насколько часто может выполняться определённая операция.
            // В данном случае мы ограничиваем частоту обращений к yesno.wtf/api,
            // дожидаясь завершения печати вопроса перед отправкой ajax-запроса.
            // Узнать больше о функции _.debounce (и её родственнице _.throttle),
            // можно в документации: https://lodash.com/docs#debounce
            this.test = 'test';
        },
        methods:{
            add_blog(){
                if(this.status_form_blog == false){
                    this.status_form_blog = true;
                }else{
                    this.status_form_blog = false;
                }
            },

            submit_add_blog(){
                axios({
                    method: 'post',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: '?action=site/blogform',
                    data: {
                        author: this.author,
                        matter: this.matter,
                        subject:this.subject
                    }
                })
                    .then(response => (this.server = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });


                this.reload_page();
                this.set_post();
                this.get_blog();
            },

            sort_author(){
                axios({
                    method: 'post',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: '?action=site/blogsort',
                    data: {
                        sort:this.sort,
                        column:this.column
                    }
                })
                .then(response => (this.let_get_blog = response.data))
                .catch(function (error) {
                    console.log(error);
                });
                this.trigger_sort;
            },

// записываем данные в базу данных
            set_post(){
                axios({
                   method:'post',
                   headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url: '?action=site/blogform',
                    data:{
                        'author': this.author,
                        'matter': this.matter,
                        'subject':this.subject
                    }
                }).then(response => (this.let_set_blog = response.data))
                  .catch(function (error) {
                        console.log(error);
                    });
            },

            reload_page:function(){
                if(this.server){
                    console.log(this.server);
                }else{
                    app_blog.author = '';
                    app_blog.matter= '';
                    app_blog.subject = '';
                }
            },

            get_blog(){
                // вывод двнных на экран
                axios({
                    method: 'post',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: '?action=site/bloglists',
                })
                    .then(response => (this.let_get_blog = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });
            }

        },
        computed: {

            // set_post(){
            //     axios
            //         .post('?action=site/blogform', {'author': this.author, 'matter': this.matter, 'subject':this.subject})
            //         .then(response => (this.server = response.data)); // Результат ответа от сервера
            //     console.log(this.server);
            // },

            trigger_sort(){
                if(this.sort == 'DESC'){
                    this.sort = 'ASC';
                }else{
                    this.sort = 'DESC';
                }
            },

            test_test(){
                alert(this.sort);
            }
        },
        mounted:function(){
            axios({
                method: 'post',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: '?action=site/bloglists',
            })
                .then(response => (this.let_get_blog = response.data))
                .catch(function (error) {
                    console.log(error);
                });
        }
    });
</script>