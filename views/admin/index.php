<div class="container">
    <div id="app_index">
    <div class="row ">
        <div class="col-sm-4">
            <figure class="figure">
                <template v-if="let_image?let_image:let_image_bool">
                    <img v-bind:src="let_image?let_image:let_image_bool" class="figure-img img-fluid rounded" alt="<?= $params['avatar']?>" id="avatar" style="width: 400px; height: 300px" title="<?= $params['avatar']?>">
                </template>

                <template v-if="let_image?let_image:let_image_bool">
                    <div align="center">
                        <a v-bind:href="'?action=admin/deleteavatar&image=' + create_link_delete" @click.prevent="delete_avater($event)">Delete Avatar</a>
                    </div>
                </template>
                <template v-if="let_delete_image_info">
                    <div class="alert alert-info" role="alert">
                        Удалить аватар не получилось. Что то пошло не так.
                    </div>
                </template>
                <figcaption class="figure-caption">A caption for the above image.</figcaption>
            </figure>
        </div>
        <div class="col-sm-8">
            <div class="text-center"><p class="font-weight-bold">Персональные данные пользователя</p></div>
            <div class="pt-5">
                <div class="col-sm-6">
                    <p class="text-center text-uppercase"><strong>Имя</strong></p>
                    <p class="text-center"><strong>Год рождения</strong></p>
                    <p class="text-center text-uppercase"><strong>Город</strong></p>
                    <p class="text-center text-uppercase"><strong>Родной язык</strong></p>
                    <p class="text-center text-uppercase"><strong>Email</strong></p>
                    <p class="text-center text-uppercase"><strong>Телефон</strong></p>
                    <p class="text-center text-uppercase"><strong>Пол</strong></p>
                </div>
                <div class="col-sm-6">
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['author']}}</strong></p>
                    <p class="text-center"><strong>{{let_personal_date['old_year']}}</strong></p>
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['city']}}</strong></p>
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['language']}}</strong></p>
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['email']}}</strong></p>
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['telephone']}}</strong></p>
                    <p class="text-center text-uppercase"><strong>{{let_personal_date['gender']}}</strong></p>
                </div>
            </div>
        </div>
    </div>
        <a href="#" class="nav nav-link" @click="trigger_form">Редактировать персональные данные пользователя</a>
    <div class="row">
        <template v-if="let_trigger_form">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row no-gutters block-9">
                        <div class="col-md-12 order-md-last d-flex">
                            <form method="post" class="bg-light p-4 p-md-5 contact-form" @submit.prevent="submit_update_personal_date">
                                <div class="form-group">
                                    <label for="exampleInputLogin">Your Name</label>
                                    <input type="text" class="form-control" placeholder="Your Name" id="exampleInputLogin" v-model="let_name" v-bind:style="{border:let_border_name}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputOldYear">Your Old Year</label>
                                    <input type="text" class="form-control" placeholder="Your Old_Year" id="exampleInputOldYear" v-model="let_old_year" v-bind:style="{border:let_border_old_year}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputCity">Your City</label>
                                    <input type="text" class="form-control" placeholder="Your City" id="exampleInputCity" v-model="let_city" v-bind:style="{border:let_border_city}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputLanguage">Your Language</label>
                                    <input type="text" class="form-control" placeholder="Your Language" id="exampleInputLanguage" v-model="let_language" v-bind:style="{border:let_border_language}">
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label for="exampleInputFile">Your File</label>-->
<!--                                    <input type="file" class="form-control" id="exampleInputFile" multiple @change="handleFileChange($event)">-->
<!--                                </div>-->
                                    <template v-if="disabled">
                                        <input type="submit" type="button" class="btn btn-primary py-3 px-5" value="Update Personal Date" >
                                    </template>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
             </div>
        </template>
    </div>
    <template>
        <div class="container">
            <div class="large-12 medium-12 small-12 cell">
                <label>File
                    <input type="file" id="file" ref="file" @change="handleFileUpload"/>
                </label>
                <button @click="submitFile">Submit</button>
            </div>
        </div>
    </template>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    "use strict"; // строгий вариант this

    let index = new Vue({
       el:'#app_index',
       data:{
           let_trigger_form:false,
           let_name:'',
           let_old_year:'',
           let_city:'',
           let_language:'',
           let_disabled:false,
           let_server_response:'',
           let_border_name:'',
           let_border_old_year:'',
           let_border_city:'',
           let_border_language:'',
           let_id:'<?= $params['get_array_user']['id']?>',
           let_personal_date:'',
           let_file:'',
           let_image_bool:'',
           let_image:'',
           let_del_image:'',
           let_delete_image_info:false,
       },
        methods:{
            submitFile(){
                let formData = new FormData();
                formData.append('file', this.let_file);

                axios.post( '?action=admin/createimage',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then((response) => {
                    this.let_image = response.data.path_image
                })
                    .catch(function(error){
                        console.log(error);
                    });
                this.clear_upload();

            },

            handleFileUpload(){
                this.let_file = this.$refs.file.files[0];
                // не отрабатывает (потому что не пишет данные в баззу)
                // this.let_file.push(this.$refs.file.files[0]);
                // if(this.let_file['1'] != undefined){
                //     this.let_file.splice(0, 1)
                // }
                // this.let_image = this.let_file['0']['name'];
                // this.let_image = "modules/admin/web/images/avatars/1_igor makarev/" +  this.let_image
            },

            clear_upload(){
                this.let_image = ''
                this.$refs.file.value = null;
            },

            delete_avater(){
               this.let_del_image = event.target.href.split('&');
               this.let_del_image = this.let_del_image[1].split('=')

               axios({
                   method: 'post',
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                   url: '?action=admin/deleteavatar',
                   data:{
                       'image':this.let_del_image[1]
                   },
               })
                   .then(response => (this.let_delete_image_info = response.data))

                   .catch(function (error) {
                       console.log(error);
                   });
               this.let_image = '';
                this.let_image_bool = ''
          },

            trigger_form(){
              this.let_trigger_form = !this.let_trigger_form;
            },

            submit_update_personal_date(){
                // axios
                //     .post('?action=admin/updatepersonaldate', {'name':this.let_name, 'old_year':this.let_old_year, 'city':this.let_city, 'language':this.let_language})
                //     .then(response => (this.let_server_response = response.data));
                //     //.then(response => (console.log(response.data)));

                axios
                    .post('?action=admin/getpersonaldate', {'id':this.let_id})
                    .then(response => this.let_personal_date = response.data);
                this.disabled;
                this.update_personal_date_post();
                this.reload_page();
                this.personal_date()
            },

            reload_page(){
                if(this.let_server_response){
                    console.log(this.let_server_response);
                }else{
                    this.let_name = '';
                    this.let_old_year = '';
                    this.let_city = '';
                    this.let_language = '';
                }
            },

            update_personal_date_post(){
                axios({
                    method: 'post',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: '?action=admin/updatepersonaldate',
                    data:{
                        'name':this.let_name,
                        'old_year':this.let_old_year,
                        'city':this.let_city,
                        'language':this.let_language,
                        'image':this.let_image,
                    },
                })
                    .then(response => (this.let_server_response = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            personal_date:function(){
                    axios({
                        method: 'post',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: '?action=admin/getpersonaldate',
                        data:{
                            'id':this.let_id
                        },
                    })
                        .then(response => (this.let_personal_date = response.data))
                        .catch(function (error) {
                            console.log(error);
                        });
                }

        },
        computed:{

           create_link_delete(){
               if(this.let_image != '') {
                   this.let_image = this.let_image
                   return this.let_image
               } else if(this.let_image_bool != ''){
                   this.let_image_bool = this.let_image_bool
                   return this.let_image_bool
               } else{
                   return null
               }

           },

           disabled(){
               this.border_empty;
               this.border_no_empty;

               if(this.let_name != '' || this.let_old_year != '' || this.let_city != '' || this.let_language != '' || this.let_length > 0){
                   return this.let_disabled = true;
               }else{
                    return this.let_disabled = false;
               }
           },

            border_empty(){
               if(this.let_name != ''){
                   this.let_border_name = '2px green solid';
               }
                if(this.let_old_year != ''){
                    this.let_border_old_year = '2px green solid';
                }
                if(this.let_city != ''){
                    this.let_border_city = '2px green solid';
                }
                if(this.let_language != ''){
                    this.let_border_language = '2px green solid';
                }
            },
            border_no_empty(){
                if(this.let_name == ''){
                    this.let_border_name = '';
                }
                if(this.let_old_year == ''){
                    this.let_border_old_year = '';
                }
                if(this.let_city == ''){
                    this.let_border_city = '';
                }
                if(this.let_language == ''){
                    this.let_border_language = '';
                }
            }
        },
        mounted:function() {
            axios({
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '?action=admin/getpersonaldate',
                data: {
                    'id': this.let_id
                },
            })
                .then(response => (this.let_personal_date = response.data))
                .catch(function (error) {
                    console.log(error);
                });


            axios({
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '?action=admin/getavatar',
            })
                .then(response => (this.let_image_bool = response.data.path_image))
                .catch(function (error) {
                    console.log(error);
                });
        },

    });
</script>