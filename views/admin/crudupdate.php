<div class="container">
    <div id="crud_update">
        <!--    start form    -->
        <!-- start Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Name table - {{let_name_table}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-6 order-md-last d-flex">
                                        <form method="post" class="bg-light p-4 p-md-2 contact-form" @submit.prevent="update_migration(let_name_table)">
                                            <div class="form-group">
                                                <div class="">
                                                    <label v-bind:for="'validationDefaultSelected'">По умолчанию</label>
                                                    <select class="form-control" v-bind:id="'validationDefaultSelected'" v-model="let_selected_value">
                                                        <option v-for="let_selected in let_selected_alert_table">{{let_selected}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUpdate">Update Table</label>
                                                <input type="text" class="form-control" placeholder="Update Table" id="exampleInputUpdate" v-model="let_set_params_alter_table">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Alter Table" class="btn btn-primary py-3 px-5" name="add_project">
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="close_modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <!--    start form lists field table   -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModalTableFilds" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Name table - {{let_name_table_field}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-6 order-md-last d-flex">
                                        <ul>
                                            <li v-for="(info_field, key) in let_request_server_name_table_field">{{let_request_server_name_table_field[key]}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/settings">Back</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/crud">Create</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" >Read</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/crudupdate" >Update</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/cruddelete">Delete</a>
            </li>
        </ul>
        <br />

        <div class="list-group col-md-5 order-md-last d-flex">
            <?php foreach ($params['lists_tables'] as $key => $value):?>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#exampleModal"
                                    @click="name_table('<?= $value['Tables_in_tesla']?>')" >
                                <?= $value['Tables_in_tesla']?>
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#exampleModalTableFilds"
                                    @click="lists_field_table('<?= $value['Tables_in_tesla']?>')" >
                                <?= $value['Tables_in_tesla']?> - Structure
                            </button>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    let crud_update = new Vue({
        el:'#crud_update',
        data:{
            let_name_table:'',
            let_selected_alert_table:['ADD', 'DROP'],
            let_selected_value:'ADD',
            let_name_table_form_project:'',
            let_name_table_field:'',
            let_set_params_alter_table:'',
            let_request_server_add_alter_table:'',
            let_request_server_name_table_field:'',
        },
        methods:{
            name_table(name_table){
                this.let_name_table = name_table
            },

            update_migration(tables){
                this.$refs.close_modal.click()

                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/updatemigration',
                    data:{
                        'name_table':tables,
                        'params_alter_table':this.let_set_params_alter_table,
                        'let_selected_value':this.let_selected_value,
                    }
                })
                    //.then(response => this.let_request_server_add_alter_table = response.data)
                    .then(response => {console.log(response.data)
                        alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                this.let_set_params_alter_table = ''
            },

            lists_field_table(table){
                this.let_name_table_field = table

                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/listsfieldtable',
                    data:{
                        'table_name':table,
                    }
                })
                    .then(response => this.let_request_server_name_table_field = response.data)
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }
    });
</script>

