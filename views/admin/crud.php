<div class="container">
    <div id="app_crud">

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/settings">Back</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/crud">Create</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" >Read</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/crudupdate">Update</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/cruddelete">Delete</a>
            </li>
        </ul>
        <br />

        <form method="POST" @submit.prevent="create_table">
            <div id="form-row">
                <div class="form-group">
                    <label for="exampleInputNameTable">Name Table</label>
                    <input type="text" class="form-control" id="exampleInputNameTable" aria-describedby="emailHelp" v-model="let_name_table" ref="let_name_table">
                </div>
                <div class="form-group" v-for="(value, index) in get_array">
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationDefaultName'+(index+1)">Имя</label>
                        <input type="text" class="form-control" v-bind:id="'validationDefaultName'+(index+1)" v-model="value['Name']"
                               v-bind:ref="'validationDefaultName'+(index+1)"
                               :key="index" >
                    </div>
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationDefaultType'+(index+1)">Тип</label>
                        <input type="text" class="form-control" v-bind:id="'validationDefaultType'+(index+1)" v-model="value['Type']"
                               v-bind:ref="'validationDefaultType'+(index+1)"
                               :key="index" >
                    </div>
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationDefaultLength'+(index+1)">Длина/Значение</label>
                        <input type="text" class="form-control" v-bind:id="'validationDefaultLength'+(index+1)" v-model="value['Length']"
                               v-bind:ref="'validationDefaultLength'+(index+1)"
                               :key="index" >
                    </div>
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationDefault'+(index+1)">По умолчанию</label>
                        <select class="form-control" v-bind:id="'validationDefault'+(index+1)" v-model="value['Selected_D']" :key="index">
                            <option>---</option>
                            <option>Нет</option>
                            <option>Как определено</option>
                            <option>Null</option>
                            <option>Current_TIMESTAMP</option>
                        </select>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationTooltipSelected'+(index+1)">Индекс</label>
                        <select class="form-control" v-bind:id="'validationTooltipSelected'+(index+1)" v-model="value['Selected_S']" :key="index">
                            <option>---</option>
                            <option>PRIMARY</option>
                            <option>UNIQUE</option>
                            <option>INDEX</option>
                            <option>FULTEXT</option>
                            <option>SPATIAL</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label" v-bind:for="'disabledFieldsetCheck'+(index+1)">
                                A_I
                            </label>
                            <br />
                            <input class="нного" type="checkbox" v-bind:id="'disabledFieldsetCheck'+(index+1)" v-model="value['Check']">
                        </div>
                    </div>

                    <!--                    <label v-bind:for="'exampleInput_Row_'+(key+1)">Name Row</label>-->
                    <!--                    <input type="text" class="form-control" v-bind:id="'exampleInput_Row_'+(key+1)" v-model="let_inputs[index]">-->

                    <!--                    <label v-bind:for="'exampleInput_'+input['Row_'+(index+1)]">Name Row</label>-->
                    <!--                    <input type="text" class="form-control" v-bind:id="'exampleInput_'+input['Row_'+(index+1)]" v-model="input['Row_'+(index+1)]" >-->

                    <!--                    <label v-bind:for="`exampleInput_${input[`"'Row_'+(index+1)+"`]}`">Name Row</label>-->
                    <!--                    <input type="text" class="form-control" v-bind:id="`${input["'Row_'+(index+1)"]}`" v-model="${input["'Row_'+(index+1)"]}`" >-->
                </div>
                <br />

                <div class="p-5">
                    <button class="btn btn-success" @click.prevent="add_row(Object.keys(get_array).length)">Add Row</button>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary" >Create Table</button>
            </div>
        </form>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    // error pokaz
    // Vue.config.errorHandler = function(err, vm, info) {
    //     console.log(`Error: ${err.toString()}\nInfo: ${info}`);
    // }
    //
    // Vue.config.warnHandler = function(msg, crud, trace) {
    //     console.log(`Warn: ${msg}\nTrace: ${trace}`);
    // }

    // window.onerror = function(message, source, line, column, error) {
    //     console.log('ONE ERROR HANDLER TO RULE THEM ALL:', message);
    // }
    //
    // Vue.config.errorHandler = function(err, vm, info) {
    //     //oopsIDidItAgain();
    //     console.log(`Error: ${err.toString()}\nInfo: ${info}`);
    // }

    let crud = new Vue({
        el:'#app_crud',
        data:{
            let_name_table:'',
            let_inputs:[
                {'Name': '', 'Type':'', 'Length':'', 'Selected_D':'---', 'Selected_S':'---', 'Check':false},
                {'Name': '', 'Type':'', 'Length':'', 'Selected_D':'---', 'Selected_S':'---', 'Check':false},
                {'Name': '', 'Type':'', 'Length':'', 'Selected_D':'---', 'Selected_S':'---', 'Check':false}
            ], //new Set(),
            let_mySetChangeTracker:1,
            let_selected:'',
            // update_migration
            id:'',
        },

        // errorCaptured(err,vm,info) {
        //     console.log(`cat EC: ${err.toString()}\ninfo: ${info}`);
        //     return false;
        // },

        methods:{
            add_row(item){
                //template rows
                let let_key_obj = item+1
                this.let_inputs.push({'Name': '', 'Type':'', 'Length':'', 'Selected_D':'---', 'Selected_S':'---', 'Check':false},)
                this.let_mySetChangeTracker += 1

            },

            create_table(){
                //console.log(Object.values(this.let_inputs))
                //console.log(this.$refs)
                if(this.validate_data()){
                    axios({
                        method: 'post',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '?action=admin/migration',
                        data: {
                            'name_table':this.let_name_table,
                            'array':this.let_inputs,
                        },
                    })
                        .then(response => {
                                if(typeof response.data == "string") {
                                    alert(response.data)
                                }
                                this.let_name_table = ''
                                this.get_array.forEach(function(value){
                                    value['Name'] = ''
                                    value['Type'] = ''
                                    value['Length'] = ''
                                })
                            })
                        .catch(function (error) {
                            console.log(error);
                        });
                }else{
                    alert('no no')
                }

            },

            remove_class(str_string, i, refs){
                let name = str_string+(i+1)
                refs[name][0].classList.remove('color_border_red')
            },

            add_class(str_string, i, refs){
                let name = str_string+(i+1)
                refs[name][0].classList.add('color_border_red')

            },

            validate_data(){
                let validate = []
                let refs = this.$refs
                let self = this

                if(this.let_name_table == ""){
                    this.$refs.let_name_table.classList.add('color_border_red')
                } else {
                    this.$refs.let_name_table.classList.remove('color_border_red')
                }

                this.let_inputs.forEach(function (item, i, array) {
                    // class remove color_border_red
                    if(item["Name"] != ""){
                        self.remove_class('validationDefaultName', i, refs)
                    }
                    if(item["Type"] != ""){
                        self.remove_class('validationDefaultType', i, refs)
                    }
                    if(item["Length"] != ""){
                        self.remove_class('validationDefaultLength', i, refs)
                    }

                    if((item["Name"] != "" && item["Type"] != "" && item["Length"] != "") || (item["Name"] != "" && item["Type"] == "text")){
                        validate[i] = true
                    }else{
                        // class add color_border_red
                        if(item["Name"] == ""){
                            self.add_class('validationDefaultName', i, refs)
                        }
                        if(item["Type"] == ""){
                            self.add_class('validationDefaultType', i, refs)
                        }
                        if(item["Length"] == ""){
                            self.add_class('validationDefaultLength', i, refs)
                        }

                        validate[i] = false
                    }
                })

                let len = 0
                validate.forEach(function(item, i){
                    if(validate[i] == true){
                        len++
                    }
                })

                if(len == Object.keys(validate).length){
                    return true
                } else {
                    return false
                }
            }
        },

        computed:{
            get_array(){
                // не отрабатывает во vue.js
                //this.let_inputs = new Map()
                //this.inputs.set(`Row_${this.inputs.size + 1}`,'')
                // this.let_inputs.add({'Row_1':'2'})
                // this.let_inputs.add({'Row_2':'4'})
                // this.let_inputs.add({'Row_3':'5'})

                // for (let key of this.inputs.entries()) {
                //     console.log(`Ключ = ${key[0]}, значение = ${key[1]}`);
                // }

                return this.let_mySetChangeTracker && this.let_inputs
            },
        }
    });
</script>

