<div class="container">
    <div id=app_projects>

        <!-- start Modal insert into -->
        <div class="modal fade bd-example-modal-lg" id="exampleModalInsertInto" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">AUTHOR - {{let_get_selecton_id['author']}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-9 order-md-last d-flex">
                                        <form method="post" class="bg-light p-4 p-md-5 contact-form" @submit.prevent="insert_into_project()">
                                            <div class="form-group">
                                                <label for="exampleInputAuthor">Your Author</label>
                                                <input type="text" class="form-control" placeholder="Your Author" id="exampleInputAuthor" v-model="let_insert_into_author"
                                                       ref="let_insert_into_author">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputAuthor">Your Email</label>
                                                <input type="email" class="form-control" placeholder="Your Email" id="exampleInputEmail" v-model="let_insert_into_email"
                                                       ref="let_insert_into_email">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputProject">Your Project</label>
                                                <input type="text" class="form-control" placeholder="Your Project" id="exampleInputProject" v-model="let_insert_into_project"
                                                       ref="let_insert_into_project">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputCode">Your Code</label>
                                                <input type="text" class="form-control" placeholder="Your Code" id="exampleInputCode" v-model="let_insert_into_code"
                                                       ref="let_insert_into_code">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Update User" class="btn btn-primary py-3 px-5">
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="close_modal_insert_into">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <!-- start Modal views -->
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">AUTHOR - {{let_get_selecton_id['author']}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-9 order-md-last d-flex">
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-dark">EMAIL - {{let_get_selecton_id['email']}}</li>
                                            <li class="list-group-item list-group-item-dark">NAME PROJECT - {{let_get_selecton_id['project']}}</li>
                                            <li class="list-group-item list-group-item-dark">{{let_get_selecton_id['code']}}</li>
                                            <li class="list-group-item list-group-item-dark">DATE - {{let_get_selecton_id['date']}}</li>
                                        </ul>


                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="close_modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <!-- start Modal updates -->
        <div class="modal fade bd-example-modal-lg" id="exampleModalUpdates" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">PROJECT - {{len_name_project}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-9 order-md-last d-flex">
                                        <form method="post" class="bg-light p-4 p-md-5 contact-form" @submit.prevent="update_project(let_id)">
                                            <div class="form-group">
                                                <label for="exampleInputAuthor">Your Author</label>
                                                <input type="text" class="form-control" placeholder="Your Author" id="exampleInputAuthor" v-model="let_author">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputProject">Your Project</label>
                                                <input type="text" class="form-control" placeholder="Your Project" id="exampleInputProject" v-model="let_project">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputCode">Your Code</label>
                                                <input type="text" class="form-control" placeholder="Your Code" id="exampleInputCode" v-model="let_code">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Update User" class="btn btn-primary py-3 px-5">
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="close_modal_updates">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <div class="container">
            <form method="post" @submit.prevent="search_fields()">
                <div id="form-row">
                    <div class="col-md-2 mb-3">
                        <label v-bind:for="'validationTooltipSelected'">Fields</label>
                        <select class="form-control" v-bind:id="'validationTooltipSelected'" v-model="let_selected" >
                            <option v-for="value in let_column">{{value}}</option>
                        </select>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label v-bind:for="'validationDefaultSearch'">Search</label>
                        <input type="text" class="form-control" placeholder="Search" v-model="let_search"
                               v-bind:id="'validationDefaultSearch'"
                               v-bind:ref="'validationDefaultSearch'">
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary pt-3" >Search</button>
                </div>
            </form>
        </div>
        <br />
        <div style="position: absolute;  margin-left: 1.5%">
            <button class="btn btn-success pt-3" data-toggle="modal" data-target="#exampleModalInsertInto">INSERT INTO PROJECT</button>
        </div>
        <br />
        <br />
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col"  style="cursor:pointer">№</th>
                <th scope="col"  style="cursor:pointer">AUTHOR</th>
                <th scope="col"  style="cursor:pointer">EMAIL</th>
                <th scope="col"  style="cursor:pointer">PROJECT</th>
                <th scope="col"  style="cursor:pointer">CODE</th>
                <th scope="col"  style="cursor:pointer">DATE</th>
                <th scope="col">ACTIONS</th>
            </tr>
            </thead>
            <tbody>
            <template v-if="!let_response_server_load_more">
            <tr v-for="(get_data, index) in let_response_get_all_data" >
                <th scope="row">{{let_get_count_id_array + index + 1}}</th>
                <td>{{get_data['author']}}</td>
                <td>{{get_data['email']}}</td>
                <td>{{get_data['project']}}</td>
                <td>{{get_data['code'].substring(0, 150)}} <a href="#" @click="page_project(get_data['id'])" data-toggle="modal" data-target="#exampleModal">
                        <strong style="color:darkblue">читать далее</strong></a></td>
                <td>{{get_data['date']}}</td>
                <td>
                    <i class="glyphicon glyphicon-eye-open text-success" style="cursor:pointer" aria-hidden="true"
                       @click="page_project(get_data['id'])" data-toggle="modal" data-target="#exampleModal"></i>
                    <i class="glyphicon glyphicon-pencil text-info" style="cursor:pointer" aria-hidden="true"
                       @click="update_project_id(get_data['id'], get_data['project'])" data-toggle="modal" data-target="#exampleModalUpdates"></i>
                    <i class="glyphicon glyphicon-trash text-danger" style="cursor:pointer" aria-hidden="true" @click="delete_project_id(get_data['id'])"></i>
                </td>
            </tr>
            </template>

            <template v-if="let_response_server_load_more">
                <tr v-for="(get_data, index) in let_load_more_projects" >
                    <th scope="row">{{let_get_count_id_array + index + 1}}</th>
                    <td>{{get_data['author']}}</td>
                    <td>{{get_data['email']}}</td>
                    <td>{{get_data['project']}}</td>
                    <td>{{get_data['code'].substring(0, 150)}} <a href="#" @click="page_project(get_data['id'])" data-toggle="modal" data-target="#exampleModal">
                            <strong style="color:darkblue">читать далее</strong></a></td>
                    <td>{{get_data['date']}}</td>
                    <td>
                        <i class="glyphicon glyphicon-eye-open text-success" style="cursor:pointer" aria-hidden="true"
                           @click="page_project(get_data['id'])" data-toggle="modal" data-target="#exampleModal"></i>
                        <i class="glyphicon glyphicon-pencil text-info" style="cursor:pointer" aria-hidden="true"
                           @click="update_project_id(get_data['id'], get_data['project'])" data-toggle="modal" data-target="#exampleModalUpdates"></i>
                        <i class="glyphicon glyphicon-trash text-danger" style="cursor:pointer" aria-hidden="true" @click="delete_project_id(get_data['id'])"></i>
                    </td>
                </tr>
            </template>

            </tbody>
        </table>

<!--    start load project    -->
        <div style="position: absolute;  margin-left: 22%">
                <button class="btn btn-primary pt-3" @click="load_more()">LOAD MORE 10</button>
        </div>
        <br />
        <br />
<!--    end load project    -->

<!--   start pagination     -->
        <template v-if="let_flag_pagination">
            <div style="position: absolute;  margin-left: 20%" v-if="let_flag">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center" ref="let_pagination">
                        <li class="page-item"><a class="page-link" href="#" >Previous</a></li>
                        <li class="page-item"
                            :key="index"
                            v-for="(get_count_page, index) in let_response_get_count_page"
                            @click.prevent="get_page(index+1)">

                            <a class="page-link" href="#">{{get_count_page}}</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>

            <div style="position: absolute;  margin-left: 20%" v-if="!let_flag">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center" ref="let_pagination_search">
                        <li class="page-item"><a class="page-link" href="#" >Previous</a></li>
                        <li class="page-item"
                            v-for="(get_count_page, index) in let_response_get_count_page"
                            @click.prevent="get_sort_page(index+1)">
                            <a class="page-link" href="#">{{get_count_page}}</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>
        </template>

    </div>
</div>
<!--                            :ref="`let_pagination${index+1}`"-->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    let app_project = new Vue({
        el:'#app_projects',
        data:{
            let_column:[
                'author', 'email', 'project', 'date'
            ],
            let_selected:'',
            let_search:'',
            let_response_get_all_data:'',
            // pagination
            let_flag:true,
            let_set_page:'',
            let_response_get_count_page:'',
            let_get_count_id_array:'',
            let_get_selecton_id:'',
            let_flag_updates:'',
            isActive:false,
            let_active_page:'',
            // update and delete project
            let_id:'',
            len_name_project:'',
            let_author:'',
            let_project:'',
            let_code:'',
            //load_more
            let_load_more:1,
            let_response_server_load_more:false,
            let_load_more_projects:'',
            let_flag_pagination:true,
            let_page:'',
            //insert into project
            let_insert_into_author:'',
            let_insert_into_email:'',
            let_insert_into_project:'',
            let_insert_into_code:'',
        },
        methods:{
            insert_into_project(){
                if(this.let_insert_into_author != '' && this.let_insert_into_email != '' && this.let_insert_into_project != '' && this.let_insert_into_code != ''){
                    axios({
                        method:'post',
                        headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                        url:'?action=admin/insertintoproject',
                        data:{
                            'aurthor':this.let_insert_into_author,
                            'email':this.let_insert_into_email,
                            'project':this.let_insert_into_project,
                            'code':this.let_insert_into_code,
                        }
                    })
                        //.then(response =>  console.log(response.data))
                        .then(response => {
                            if(response.data){
                                this.let_insert_into_author = ''
                                this.let_insert_into_email = ''
                                this.let_insert_into_project = ''
                                this.let_insert_into_code = ''

                                this.field_validation(this.let_insert_into_author, 'let_insert_into_author')
                                this.field_validation(this.let_insert_into_email, 'let_insert_into_email')
                                this.field_validation(this.let_insert_into_project, 'let_insert_into_project')
                                this.field_validation(this.let_insert_into_code, 'let_insert_into_code')

                                this.get_all_projects()
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                    this.$refs.close_modal_insert_into.click()
                }else{
                    this.field_validation(this.let_insert_into_author, 'let_insert_into_author')
                    this.field_validation(this.let_insert_into_email, 'let_insert_into_email')
                    this.field_validation(this.let_insert_into_project, 'let_insert_into_project')
                    this.field_validation(this.let_insert_into_code, 'let_insert_into_code')

                    alert('Fill in all the form fields!!!')
                }
            },

            field_validation(variable, stroka){
                if(variable != ''){
                    this.$refs[stroka].classList.remove('color_border_red')
                }else{
                    this.$refs[stroka].classList.add('color_border_red')
                }
            },

            load_more(){
                this.let_load_more+=1

                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/loadmore',
                    data:{
                        'load_more':this.let_load_more,
                    }
                })
                    .then(response => {
                        if(this.let_load_more <= (this.$refs['let_pagination'].children.length-2)){
                            this.$refs['let_pagination'].children[this.let_load_more -1].classList.remove('active')
                            this.$refs['let_pagination'].children[this.let_load_more].classList.add('active')
                        }

                        this.let_load_more_projects = response.data
                        this.let_response_server_load_more = true
                        // if(response.data){
                        //     this.let_author=''
                        //     this.let_project=''
                        //     this.let_code=''
                        //     this.close_modal_update()
                        //     this.get_all_projects()
                        //
                        // }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });


            },

            update_project_id(index, project){
              this.len_name_project = project
              this.let_flag_updates = confirm("ARE YOU UPDATING PROJECT?")
                if(this.let_flag_updates){
                    this.let_id = index
                    //this.update_project(index)
                }else{
                    setTimeout(this.close_modal_update, 50)

                }
            },

            update_project(index){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/updateproject',
                    data:{
                        'id':index,
                        'author':this.let_author,
                        'project':this.let_project,
                        'code':this.let_code,
                    }
                })
                    //.then(response =>  console.log(response.data))
                    .then(response => { //this.let_get_selecton_id = response.data
                        if(response.data){
                            this.let_author=''
                            this.let_project=''
                            this.let_code=''
                            this.close_modal_update()
                            this.get_all_projects()

                        }
                        //alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            close_modal_update(){
                this.$refs.close_modal_updates.click()
            },

            delete_project_id(index){
                let let_flag = confirm("ARE YOU DELETING PROJECT?")
                if(let_flag){
                    axios({
                        method:'post',
                        headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                        url:'?action=admin/deleteproject',
                        data:{
                            'id':index,
                        }
                    })
                        //.then(response =>  console.log(response.data))
                        .then(response => { //this.let_get_selecton_id = response.data
                            if(response.data){
                                this.get_all_projects()
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            },

            page_project(index){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/getselectonid',
                    data:{
                        'id':index,
                    }
                })
                    //.then(response =>  console.log(response.data))
                    .then(response => { this.let_get_selecton_id = response.data
                        //alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            get_all_projects(){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/getallprojects',
                    data:{
                        'page':1,
                    }
                })
                    .then(response => { this.let_response_get_all_data = response.data
                        //alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            get_sort_page(page){
                this.let_active_page = 'let_pagination_search'

                var index = 0
                for(let property in this.$refs[this.let_active_page].children){

                    if(index < this.$refs[this.let_active_page].children.length){
                        this.$refs[this.let_active_page].children[property].classList.remove('active')
                        index++
                    }
                }

                this.$refs[this.let_active_page].children[page].classList.add('active')

                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/getsearchprojects',
                    data:{
                        'sort_page':page,
                        'column':this.let_selected,
                        'search':this.let_search
                    }
                })
                    //.then(response => (console.log(response.data)))
                    .then(response => { this.let_response_get_all_data = response.data

                        axios({
                            method:'post',
                            headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                            url:'?action=admin/getcountidarray',
                            data:{
                                'count_str':page,
                            }
                        })
                            //.then(response => (console.log(response.data)))
                            .then(response => { this.let_get_count_id_array = response.data
                                //alert(response.data)
                            })
                            .catch(function (error) {
                                console.log(error);
                            });

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            test(event){
                alert(event.target.tagName)
                //this.test() // рекурсия
                // return event.target.innerHTML
                //this.get_page(event.target.innerHTML)
            },

            event_page(event){
                let page = event.target
                this.let_flag_pagination = true
                this.let_page = page
                this.get_page(page.innerHTML)
            },

            get_page(page){
                if(this.let_flag_pagination == true){
                    this.let_active_page = 'let_pagination'
                    this.let_response_server_load_more = false
                    this.let_load_more_projects = ''

                    //for (const property in this.$refs) {
                    //console.log(`${property}: ${this.$refs[property]}`)
                    //}

                    var index = 0
                    for(let property in this.$refs[this.let_active_page].children){
                        if(index < this.$refs[this.let_active_page].children.length){
                            this.$refs[this.let_active_page].children[property].classList.remove('active')
                            index++
                        }
                    }
                    this.$refs[this.let_active_page].children[page].classList.add('active')

                    //this.$refs[active_page]['0'].classList.remove('active')
                    //console.log(this.$refs[this.let_active_page]['0'].classList)
                    //this.$refs[this.let_active_page]['0'].classList.add('active')

                    // this.$refs[this.let_active_page].children[page].classList.add('active')

                    //if(this.let_active_page == 'pagination' + page){
                    //this.$refs[active_page]['0'].classList.add('active')
                    //}else{
                    //this.$refs[active_page]['0'].classList.remove('active')
                    //}

                    axios({
                        method: 'post',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '?action=admin/getpagination',
                        data: {
                            'page': page,

                        }
                    })
                        .then(response => {
                            this.let_response_get_all_data = response.data
                            //console.log(this.let_response_get_all_data)
                            axios({
                                method: 'post',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                url: '?action=admin/getcountidarray',
                                data: {
                                    'count_str': page,
                                }
                            })
                                .then(response => {
                                    this.let_get_count_id_array = response.data
                                    //console.log(response.data)
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });

                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }

            },

            search_fields(){
                this.let_active_page = 'let_pagination_search'

                if(this.let_flag == true){
                    this.let_flag = !this.let_flag
                }

                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/getsearchprojects',
                    data:{
                        'sort_page':1,
                        'column':this.let_selected,
                        'search':this.let_search,
                    }
                })
                    //.then(response => (console.log(response.data)))
                    .then(response => { this.let_response_get_all_data = response.data
                        this.$refs['let_pagination_search'].children[1].classList.add('active')
                        axios({
                            method:'post',
                            headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                            url:'?action=admin/getcountsortpagination',
                            data:{
                                'column':this.let_selected,
                                'search':this.let_search,
                            }
                        })
                            //.then(response => (console.log(response.data)))
                            .then(response => { this.let_response_get_count_page = response.data
                                //alert(response.data)
                            })
                            .catch(function (error) {
                                console.log(error);
                            });

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },

        created:function(){
            axios({
                method:'post',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                url:'?action=admin/getallprojects',
                data:{
                    'page':1,
                }
            })
                .then(response => { this.let_response_get_all_data = response.data
                    //alert(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                });

            axios({
                method:'post',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                url:'?action=admin/getcountpagination',
            })
                //.then(response =>  console.log(response.data))
                .then(response => { this.let_response_get_count_page = response.data

                    axios({
                        method:'post',
                        headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                        url:'?action=admin/getcountidarray',
                        data:{
                            'count_str':1,
                        }
                    })
                        //.then(response =>  console.log(response.data))
                        .then(response => { this.let_get_count_id_array = response.data
                            //alert(response.data)
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                })
                .catch(function (error) {
                    console.log(error);
                });

            if(this.let_active_page == ''){
                //this.let_active_page = 'pagination1'
                //console.log(this.$refs.let_insert_into_author)
                //this.$refs[this.let_active_page]['0'].classList.add('active')
                //console.log(this.$refs['pagination1'])
            }

        },

        mounted:function() {
            axios({
                method:'post',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                url:'?action=admin/getcountpagination',
            })
                //.then(response =>  console.log(response.data))
                .then(response => { //this.let_response_get_count_page = response.data
                    this.$refs['let_pagination'].children[1].classList.add('active')
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        computed:{
            active_page(refs){
                return refs
            }
        }
    })
</script>

