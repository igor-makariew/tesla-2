<div class="container">
    <div id="listuser">
        <!-- start Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-6 order-md-last d-flex">
                                        <form method="post" class="bg-light p-4 p-md-5 contact-form" @submit.prevent="update_user()">
                                            <div class="form-group">
                                                <label for="exampleInputAuthor">Your Author</label>
                                                <input type="text" class="form-control" placeholder="Your Author" id="exampleInputAuthor" v-model="let_author">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputLogin">Your Login</label>
                                                <input type="text" class="form-control" placeholder="Your Login" id="exampleInputLogin" v-model="let_login">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail">Your Email</label>
                                                <input type="email" class="form-control" placeholder="Your Email" id="exampleInputEmail" v-model="let_email">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputRole">Your Role</label>
                                                <input type="text" class="form-control" placeholder="Your Role" id="exampleInputRole" v-model="let_role">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputAvatar">Your Avatar</label>
                                                <input type="text" class="form-control" placeholder="Your Avatar" id="exampleInputAvatar" v-model="let_avatar">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Update User" class="btn btn-primary py-3 px-5">
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="modal_close">Close</button>
<!--                        <button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- end Modal -->

        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col" @click="sort_number('id')" style="cursor:pointer">№</th>
                <th scope="col" @click="sort_number('author')" style="cursor:pointer">AUTHOR</th>
                <th scope="col" @click="sort_number('login')" style="cursor:pointer">LOGiN</th>
                <th scope="col" @click="sort_number('email')" style="cursor:pointer">EMAIL</th>
                <th scope="col" @click="sort_number('role')" style="cursor:pointer">ROLE</th>
                <th scope="col">AVATAR</th>
                <th scope="col">DELETE</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(list_user, key) in let_list_user" data-toggle="modal" data-target="#exampleModal" @click="params_id(list_user['id'])">
                <th scope="row">{{key + 1}}</th>
                <td>{{list_user['author']}}</td>
                <td>{{list_user['login']}}</td>
                <td>{{list_user['email']}}</td>
                <td>{{list_user['role']}}</td>
                <td>{{list_user['avatar']}}</td>
                <td><button type="button" class="btn btn-danger" @click="delete_user(list_user['id'])">DELETE</button></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    let listuser = new Vue({
        el:'#listuser',
        data:{
            let_list_user:'',
            let_request_delete_list_user:'',
            // update user
            let_author:'',
            let_login:'',
            let_email:'',
            let_role:'',
            let_avatar:'',
            let_params_id:'',
            let_array:{},
            // sort
            let_params_sort:false,
            let_sort:'',
            column:'',
        },
        methods:{

            sort_number(column){
                this.let_params_sort = !this.let_params_sort
                if(this.let_params_sort){
                    this.let_sort = 'DESC'
                }else{
                    this.let_sort = 'ASC'
                }

                axios({
                    method: 'post',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: '?action=admin/sortcolumn',
                    data:{
                        'sort':this.let_sort,
                        'column':column,
                    }
                })
                    .then(response => {
                        this.let_list_user = response.data

                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            },

            set_array(){
                if(this.let_author != ''){
                    this.let_array['author'] = this.let_author
                }
                if(this.let_login != ''){
                    this.let_array['login'] = this.let_login
                }
                if(this.let_email != ''){
                    this.let_array['email'] = this.let_email
                }
                if(this.let_role != ''){
                    this.let_array['role'] = this.let_role
                }
                if(this.let_avatar != ''){
                    this.let_array['avatar'] = this.let_avatar
                }
            },

            params_id(id){
                this.let_params_id = id
            },

            update_user(){
                //console.log(this.let_params_id)
                this.set_array()

                axios({
                    method: 'post',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: '?action=admin/updateuser',
                    data:{
                        'id':this.let_params_id,
                        'array':this.let_array,
                    }
                })
                    .then(response => {if(response.data == false){
                            this.list_user()
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                this.$refs.modal_close.click()
            },

            delete_user(id){
                let del_user = confirm('Are you ready delete user?')
                if(del_user){
                    axios({
                        method: 'post',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: '?action=admin/deletelistuser',
                        data:{
                            'id':id
                        }
                    })
                        .then(response => {if(response.data){
                                this.list_user()
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }

            },
            list_user(){
                axios({
                    method: 'post',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: '?action=admin/getlistuser',
                })
                    .then(response => (this.let_list_user = response.data))
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },
        computed:{

        },
        mounted:function(){
            axios({
                  method: 'post',
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  url: '?action=admin/getlistuser',
              })
                .then(response => (this.let_list_user = response.data))
                .catch(function (error) {
                        console.log(error);
                });
        }
    });
</script>
