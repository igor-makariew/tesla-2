<div class="container">
    <div id="personal_setting">

        <!-- Modal changePassword-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <section class="ftco-section contact-section ftco-no-pb pt-0 pb-2">
                            <div class="container">
                                <div class="row no-gutters block-9">
                                    <div class="col-md-6 order-md-last d-flex">
                                        <form method="post" class="bg-light p-4 p-md-2 contact-form" @submit.prevent="change_password()">
                                            <div class="form-group">
                                                <div class="">
                                                    <label v-bind:for="'validationDefaultSelectedPassword'">Password</label>
                                                    <input type="password" class="form-control" v-bind:id="'validationDefaultSelectedPassword'" v-model="let_password"
                                                           v-bind:class="[let_return_password?'color_is_invalid':'',let_return_confirm?'color_is_invalid':'']"
                                                           v-bind:ref="'validationDefaultSelectedPassword'">

                                                    <div class="invalid_feedback" v-if="let_return_password">
                                                        Please provide a valid password.
                                                    </div>
                                                    <div class="invalid_feedback" v-if="let_return_confirm">
                                                        Your passwords are different.
                                                    </div>
                                                    <br />
                                                    <label v-bind:for="'validationDefaultSelectedConfirmPassword'">Confirm Password</label>
                                                    <input type="password" class="form-control" v-bind:id="'validationDefaultSelectedConfirmPassword'" v-model="let_confirm_password"
                                                           v-bind:class="[let_return_confirm_password?'color_is_invalid':'',let_return_confirm?'color_is_invalid':'']"
                                                           v-bind:ref="'validationDefaultSelectedConfirmPassword'">

                                                    <div class="invalid_feedback" v-if="let_return_confirm_password">
                                                        Please provide a valid confirm password.
                                                    </div>
                                                    <div class="invalid_feedback" v-if="let_return_confirm">
                                                        Your passwords are different.
                                                    </div>
                                                    <br />
                                                    <label v-bind:for="'validationDefaultSelectedUniqCode'">Uniq Code</label>
                                                    <input type="text" class="form-control" v-bind:id="'validationDefaultSelectedUniqCode'" v-model="let_uniq_code"
                                                           v-bind:class="[let_return_uniq_code?'color_is_invalid':'', let_return_undifferent_uniq_code?'color_is_invalid':'']"
                                                           v-bind:ref="'validationDefaultSelectedUniqCode'">
                                                    <div class="invalid_feedback" v-if="let_return_uniq_code">
                                                        Please provide uniq code.
                                                    </div>
                                                    <div class="invalid_feedback" v-if="let_return_undifferent_uniq_code">
                                                        Please provide a valid uniq code.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
<!--                                                <label for="exampleInputUpdate">Update Table</label>-->
<!--                                                <input type="text" class="form-control" placeholder="Update Table" id="exampleInputUpdate" v-model="let_set_params_alter_table">-->
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Change Password" class="btn btn-primary py-3 px-5">
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" ref="close_modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--    end form    -->

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">Personal Setting</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/crud">CRUD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
        </ul>

        <br />
        <div class="col-sm-6">
            <form method="POST" @submit.prevent="update_personal_data('<?= $params['id_user']?>')">
            <div id="form-row">
                <div class="form-group">
                    <label for="exampleInputAuthor">Author</label>
                    <input type="text" class="form-control" id="exampleInputAuthor" aria-describedby="emailHelp" :placeholder="let_author" v-model="let_author" ref="let_author">
                </div>
                <div class="form-group">
                    <label for="exampleInputLogin">Login</label>
                    <input type="text" class="form-control" id="exampleInputLogin" aria-describedby="emailHelp" :placeholder="let_login" v-model="let_login" ref="let_login">
                </div>
                <div class="form-group">
                    <label for="exampleInputAge">Age</label>
                    <input type="text" class="form-control" id="exampleInputAge" aria-describedby="emailHelp" :placeholder="let_age" v-model="let_age" ref="let_age">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" :placeholder="let_email" v-model="let_email" ref="let_email">
                </div>
                <div class="form-group">
                    <label for="exampleInputTelephone">Telephone</label>
                    <input type="text" class="form-control" id="exampleInputTelephone" aria-describedby="emailHelp" :placeholder="let_telephone" v-model="let_telephone" ref="let_telephone">
                </div>
                <div class="form-group">
                    <label for="exampleInputLocation">Location</label>
                    <input type="text" class="form-control" id="exampleInputLocation" aria-describedby="emailHelp" :placeholder="let_location" v-model="let_location" ref="let_location">
                </div>
                <div class="form-group">
                    <label for="exampleInputLanguage">Language</label>
                    <input type="text" class="form-control" id="exampleInputLanguage" aria-describedby="emailHelp" :placeholder="let_language" v-model="let_language" ref="let_language">
                </div>

                <fieldset class="form-group">
                    <legend>Gender</legend>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="exampleInputMan">
                            <input type="radio" class="form-check-input" name="optionsRadios" id="exampleInputMan" value="Man" v-model="let_gender">
                            Man
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="exampleInputWoman">
                            <input type="radio" class="form-check-input" name="optionsRadios" id="exampleInputWoman" value="Woman" v-model="let_gender">
                            Woman
                        </label>
                    </div>
                </fieldset>
                <div>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" @click="post_code_change_password">
                        Post Code Change Password
                    </button>
                </div>

                <hr>
                <button type="submit" class="btn btn-primary">Update Personal Data</button>
            </div>
        </form>
        </div>
        <div class="col-sm-6">
            <ul class="list-group">
                <li class="list-group-item disabled" aria-disabled="true">Personal Data User</li>
                <li class="list-group-item" v-for="(value, index) in let_response_personal_data"><strong>{{index}}</strong> - {{value}}</li>
            </ul>
        </div>


<!-- start  download file       -->
<!--        <a href="#" download="modules/admin/web/images/avatars/1_igor/s01.jpg">DOWNLOAD FILE</a>-->
<!-- end  download file       -->
<!--        <a href="#" @click="download_file">DOWNLOAD FILE</a>-->


    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<!-- cdn vuelidate-->
<!--<script src="https://cdn.jsdelivr.net/npm/vuelidate@0.7.5/lib/index.min.js"></script>-->
<script>
     //Vue.use(window.vuelidate.default)

     //import {require, email} from 'vuelidate/lib/validators'

    let personal_setting = new Vue({
        el:'#personal_setting',
        data:{
            let_author:'',
            let_login:'',
            let_age:'',
            let_email:'',
            let_telephone:'',
            let_location:'',
            let_language:'',
            let_gender:'Man',
            let_password:'',
            let_confirm_password:'',
            let_uniq_code:'',
            let_request_personal_data:'',
            let_response_personal_data:'',
            // post code
            let_response_post_code:'',
            let_return_password:'',
            let_return_confirm_password:'',
            let_return_uniq_code:'',
            let_return_undifferent_uniq_code:'',
            let_return_confirm:'',
        },

        // validations:{
        //     password:{
        //         require,
        //         email
        //     }
        // },

        methods:{
            change_password(){
                this.lists_personal_data()

                if(this.let_password == ''){
                    this.let_return_password = true;
                }else{
                    this.let_return_password = false;
                }

                if(this.let_confirm_password == ''){
                    this.let_return_confirm_password = true;
                }else{
                    this.let_return_confirm_password = false;
                }

                if(this.let_password == this.let_confirm_password){
                    this.let_return_confirm = false
                }else{
                    this.let_return_confirm = true
                }

                if(this.let_uniq_code == ''){
                    this.let_return_uniq_code = true;
                }else{
                    this.let_return_uniq_code = false;
                }

                if(this.let_uniq_code == this.let_response_personal_data['uniq_code']){
                    this.let_return_undifferent_uniq_code = false
                }else{
                    this.let_return_undifferent_uniq_code = true
                }

                if(this.let_return_password == false && this.let_return_uniq_code == false
                    && this.let_return_confirm == false && this.let_return_undifferent_uniq_code == false){
                    this.$refs.close_modal.click()

                    axios({
                        method:'post',
                        headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                        url:'?action=admin/confirmpassword',
                        data:{
                            'id':'<?= $params["id_user"]?>',
                            'password':this.let_password,
                            'uniq_code':this.let_uniq_code,
                        }
                    })
                        .then(response => {
                            console.log(response.data)

                        })
                        //.then(response => { this.let_response_post_code = response.data
                        //alert(response.data)
                        //})
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            },


            post_code_change_password(){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/postuniqcode',
                    data:{
                        'id':'<?= $params["id_user"]?>',
                    }
                })
                    //.then(response => console.log(response.data))
                    .then(response => { this.let_response_post_code = response.data
                        this.lists_personal_data()
                        //alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            update_personal_data(id_user){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/setpersonaldata',
                    data:{
                        'id':id_user,
                        'author':this.let_author,
                        'login':this.let_login,
                        'old_year':this.let_age,
                        'email':this.let_email,
                        'telephone':this.let_telephone,
                        'city':this.let_location,
                        'language':this.let_language,
                        'gender':this.let_gender,

                    }
                })
                    //.then(response => console.log(response.data))
                    .then(response => {
                        if(response.data){
                            alert('Вы успешно обновили свои персональные данные!')
                            this.let_author=''
                            this.let_login=''
                            this.let_age=''
                            this.let_email=''
                            this.let_telephone=''
                            this.let_location=''
                            this.let_language=''
                            this.let_gender='Man'
                            this.lists_personal_data()
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },

            lists_personal_data(){
                axios({
                    method:'post',
                    headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                    url:'?action=admin/getpersonaldate',
                    data:{
                        'id':'<?= $params["id_user"]?>',
                    }
                })
                    .then(response => { this.let_response_personal_data = response.data
                        //alert(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
// по скачиванию файла
            download_file(){
                axios({
                    method:'get',
                    url:'?action=admin/downloadfile',
                    //responseType:'blob',
                    //let_src:'modules/admin/web/images/avatars/1_igor/s01.jpg'
                })
                    .then(response => { console.log(response.data)
                        let filename = '123' + (Math.floor(Date.now() / 1000)) + '.xlsx';
                        //let filename = response.data
                        let url = window.URL.createObjectURL(new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }))
                        //let url = window.URL.createObjectURL(new Blob([response.data], { type: 'image/jpeg' }))
                        let link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', filename);
                        document.body.appendChild(link);
                        link.click()
                        link.remove()

                        //console.log(filename)
                        //console.log(url)
                        //console.log(link)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },

        created:function(){
            axios({
                method:'post',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                url:'?action=admin/getpersonaldate',
                data:{
                    'id':'<?= $params["id_user"]?>',
                }
            })
                .then(response => { this.let_response_personal_data = response.data
                    //alert(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                });



        }
    })
</script>
