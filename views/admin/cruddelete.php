<div class="container">
    <div id="crud_delete">

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/settings">Back</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="?action=admin/crud">Create</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" >Read</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?action=admin/crudupdate" >Update</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Delete</a>
            </li>
        </ul>
        <br />

        <div class="m-5" v-for="(value, index) in let_request_server_name_table">
                <div class="col-sm-8 h4 text-info">{{value['Tables_in_tesla']}}</div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-danger" @click="delete_table(value['Tables_in_tesla'])">Delete Table</button>
                </div>
        </div>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    let crud_delete = new Vue({
        el:'#crud_delete',
        data:{
            let_request_server_name_table:'',
            let_delete_table:'',
        },
        methods:{
            delete_table(name_table){
                this.let_delete_table = confirm('Are you want delete table - ' + name_table)
                if(this.let_delete_table){
                    axios({
                        method:'post',
                        headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                        url:'?action=admin/deletetable',
                        data:{
                            'name_table' : name_table,
                        }
                    })
                        .then(response => {
                            if(typeof response.data == "string"){
                                alert(response.data)
                            }else{
                                this.let_request_server_name_table = response.data
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            }
        },
        mounted:function(){
            axios({
                method:'post',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                url:'?action=admin/listtablesindb',
            })
                .then(response => this.let_request_server_name_table = response.data)
                .catch(function (error) {
                    console.log(error);
                });
        }
    })
</script>
