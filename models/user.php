<?php
    require_once ('core/model.php');

    class User extends Model{
        public $array_user = array();

        public function __construct(){
            parent::__construct();
            if(isset($_SESSION['get_user']['id'])){
                $user_id = $_SESSION['get_user']['id'];
                if(!$this->get_id($user_id)){
                    unset($_SESSION['get_user']);
                }
            }
        }

        function add_user($author, $login, $email, $password, $role){
            $password_hash = password_hash($password, PASSWORD_DEFAULT);

            $query = sprintf("INSERT INTO `users` (`author`, `login`, `email`, `password`, `role`) VALUES ('%s', '%s', '%s', '%s', '%s')",
                                    mysqli_real_escape_string($this->get_link() ,$author), mysqli_real_escape_string($this->get_link(), $login),
                                    mysqli_real_escape_string($this->get_link(), $email), mysqli_real_escape_string($this->get_link(), $password_hash),
                                    mysqli_real_escape_string($this->get_link(), USER));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function login_user($login, $password){
            if($this->load_user($this->get_user($login, $password))){
                return true;
            }else{
                return false;
            }
        }

        function get_user($login, $password){
            $query = sprintf("SELECT * FROM `users` WHERE `login` = '%s'", mysqli_real_escape_string($this->get_link(), $login));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            if(password_verify($password, $array_result['password'])){
                $_SESSION['get_user'] = $array_result;
                return $array_result;
            } else {
                return false;
            }
        }

        function load_user($array){
            if(!empty($array)){
                if($this->init_user($array)){
                    return true;
                } else{
                    return false;
                }
            }else{
                return false;
            }
        }

        function init_user($array){
            foreach($array as $key => $value){
                $this->array_user[$key] = $value;
            }
            if(!empty($this->array_user)){
                return true;
            }else{
                return false;
            }
        }

        function logout_user($array){
            if(isset($_SESSION['get_user'])){
                unset($_SESSION['get_user']);
                if($this->unset_get_user($array)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        function unset_get_user($array){
            if(!empty($array)){
                foreach($array as $key => $value){
                    unset($array[$key]);
                }
                if(empty($array)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }

        function get_id($id){
            $query = sprintf("SELECT * FROM `users` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            if($this->load_user($array_result)){
                return true;
            }else{
                return false;
            }
        }

        function get_array_user(){
            return $this->array_user;
        }

        // преобразует числовое значение ролей в строки
        function turn_number_into_string(){
            $array_const = [1=>SUPERADMIN, 2=>ADMIN, 3=>MODERATOR, 5=>USER, 10=>GUEST];
            $str_const = '';

            if(isset($_SESSION['get_user'])) {
                foreach ($array_const as $key => $value) {
                    if ($key == $_SESSION['get_user']['role']) {
                        $str_const = $value;
                        return $str_const;
                    }
                }
            }else{
                return GUEST;
            }
        }

    }