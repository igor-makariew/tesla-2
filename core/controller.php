<?php
    require_once ('configuration.php');

    class Controller extends Configuration {

        public $params;
        public $core;
        public function __construct($core){
            $this->core = $core;
        }

        function render($view){
            $params = $this->params;

            ob_start();
            include $view;
            $content = ob_get_clean();
            $title = $this->params['title'];
            $core = $this->core;

            include $this->get_layout();
        }
    }