<?php
    require_once ('controllers/site_controller.php');
    require_once ('modules/url/url.php');
    require_once ('models/user.php');
    require_once ('models/project.php');
    require_once ('models/pagination.php');
    require_once ('models/blog.php');

    require_once ('modules/admin/controllers/admin_controller.php');
    require_once ('modules/admin/models/personal_date.php');
    require_once ('modules/admin/models/images.php');
    require_once ('modules/admin/models/listuser.php');
    require_once ('modules/admin/models/migration.php');
    require_once ('modules/admin/models/projects.php');

    class Core{
        public $active_controller = '';
        public $active_action = '';
        private $Objects = array();

        public function __construct(){
            if(!isset($_SESSION)){
                session_start();
            }

            $this->Objects['site_controller'] = new Site($this);
            $this->Objects['user_model'] = new User($this);
            $this->Objects['project_model'] = new Project($this);
            $this->Objects['pagination_model'] = new Pagination($this);
            $this->Objects['blog_model'] = new Blog($this);
            $this->Objects['admin_controller'] = new Admin($this);
            $this->Objects['personal_date_model'] = new Personaldate($this);
            $this->Objects['images_model'] = new Images($this);
            $this->Objects['listuser_model'] = new Listuser($this);
            $this->Objects['migration_model'] = new Migration($this);
            $this->Objects['projects_model'] = new Projects($this);
        }

        function get_core(){
            return $this->Objects;
        }

        function start(){
            $this->active_controller = $this->Objects['site_controller'];
            $action = filter_input(INPUT_GET, 'action');
            $this->Objects['url'] = new Url($action);

            if($this->Objects['url']->get_name_controller() != ''){
                if(class_exists(ucfirst ($this->Objects['url']->get_name_controller()))){
                    $this->active_controller = ucfirst($this->Objects['url']->get_name_controller());
                    $this->active_controller = new $this->active_controller($this);
                }
            }

            if($this->Objects['url']->get_name_action() != ''){
                if(method_exists($this->active_controller, 'action'.$this->Objects['url']->get_name_action())){
                    $this->active_controller->{'action'.ucfirst($this->Objects['url']->get_name_action())}();
                }else{
                    $this->active_controller->action404();
                }
            }else{
                $this->active_controller->actionIndex();
            }
        }
    }
