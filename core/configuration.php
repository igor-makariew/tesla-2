<?php
    class Configuration{

        public $array_css = array();
        public $array_js = array();
        public $array_fonts = array();
        public $layout = '';

        function add_css($css){
            array_push($this->array_css, $css);
        }

        function get_css(){
            return $this->array_css;
        }

        function create_css_style(){
            $content = '';
            foreach ($this->get_css() as $item){
                $content .= "<link href='{$item}' type='text/css' rel='stylesheet'>";
            }
            return $content;
        }

        function add_js($js){
            array_push($this->array_js, $js);
        }

        function get_js(){
            return $this->array_js;
        }

        function create_js_script(){
            $content = '';
            foreach ($this->get_js() as $item){
                $content .= "<script src='{$item}'></script>";
            }
            return $content;
        }

        function add_fonts($fonts){
            array_push($this->array_fonts, $fonts);
        }

        function get_fonts(){
            return $this->array_fonts;
        }

        function create_fonts(){
            $content = '';
            foreach ($this->get_fonts() as $item){
                $content .= "<link href='{$item}' type='text/css' rel='stylesheet'>";
            }
            return $content;
        }
        // путь шаблона
        function set_layout($layout){
            $this->layout =  $layout;
        }

        function get_layout(){
            return $this->layout;
        }
    }